'use strict';

module.exports = {
	isEmpty: isEmpty,
	isBlacklisted: isBlacklisted,
};

let ArrayList = require('arraylist');
let blacklistedCompany = new ArrayList();

/**
 * proof as empty
 * @returns {*|boolean}
 */
function isEmpty(){
	return blacklistedCompany.isEmpty();
}

/**
 * Company ist Blacklisted
 * @param item
 * @returns {*|boolean}
 */
function isBlacklisted(item){
	return blacklistedCompany.contains(item);
}