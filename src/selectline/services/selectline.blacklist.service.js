'use strict';

module.exports = {
	init: init,
	isBlacklisted: isBlacklisted
};

let blacklistedCompany;

/**
 *
 * @returns {Promise<void>}
 */
async function init(){
	blacklistedCompany = require('../model/selectline.blacklist.model');
	await fillModel();
}

/***
 *
 * @param item
 * @returns {*}
 */
function isBlacklisted(item){
	if (blacklistedCompany.isEmpty()){
		return false;
	}
	if (item == null || item === undefined || item.length < 1){
		return undefined;
	}
	return blacklistedCompany.isBlacklisted(clean(item));
}

/***
 *
 * @param item
 * @returns {string}
 */
function clean(item){
	return item.toLowerCase();
}

/**
 *
 * @returns {Promise<void>}
 */
async function fillModel(){
	//TODO
}