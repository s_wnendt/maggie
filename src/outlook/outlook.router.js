'use strict';

let path = '/outlook/';

module.exports = function (app) {
	let outlookController = require('./controllers/outlook.controller');
	app.post(path+'config/', outlookController.stateAction);
	app.post(path+'info/', outlookController.infoAction);
};