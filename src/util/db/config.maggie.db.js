'use strict';

let configMaggie = module.exports =
	{
		user: 'postgres',
		host: '172.22.54.32',
		database: 'maggie_prod',
		port: 5432,
	};