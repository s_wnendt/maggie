'use strict';

module.exports = {
	DEFAULT_MAIL : 'a.schnapke@cybertrading.de',
	DOMAIN_CYBER : 'cybertrading.de',
	REPORT_SUBJECT: '[Horst 42] Cybertrading Portfolio Report',

	BROKER_MAIL: 'c.knittel@cybertrading.de',
	BROADCASTS_MAIL: 'broadcast@cybertrading.de',
	DEV_MAIL: 'a.schnapke@cybertrading.de',
	HEAD_OF_SALES_MAIL: 's.gruenheid@cybertrading.de',
	CEO_MAIL: 'f.niemann@cybertrading.de',

	PRODUCTMANAGER_MAIL: 'h.poetsch@cybertrading.de',

	CACHING_TIME_IN_HOURS: 24
};