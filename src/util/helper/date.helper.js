'use strict';

module.exports = {
	calcDays: calcDays,
	calcHours: calcHours,
	diffHoursFromDate:diffHoursFromDate,
	compareDates: compareDates
};

/**
 * Just to calculate the absolute difference between 2 dates
 * @returns {string}
 * @param begin
 * @param end
 */
function calcDays(begin, end){
	if (begin === undefined || begin === null || end === undefined || end === null)
		return '';
	if (Array.isArray(begin) || Array.isArray(end)){
		return '';
	}
	let date = new Date(begin);
	let today = new Date(end);

	let timeDiff = Math.abs(date.getTime() - today.getTime());
	let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
	if (Number.isNaN(diffDays)){
		return '';
	}
	return diffDays + '';
}

/**
 * Just to calculate the absolute difference in hours between 2 dates
 * @param begin
 * @param end
 * @returns {number}
 */
function calcHours(begin, end){
	if (begin === undefined || begin === null || end === undefined || end === null)
		return Number.NaN;
	if (Array.isArray(begin) || Array.isArray(end)){
		return Number.NaN;
	}
	let date = new Date(begin);
	let today = new Date(end);

	let timeDiff = Math.abs(date.getTime() - today.getTime());
	let diffDays = Math.ceil(timeDiff / (1000 * 60 * 60));
	if (Number.isNaN(diffDays)){
		return Number.NaN;
	}
	return diffDays;
}

/***
 * diff hours from given date
 * @param date
 * @param hours
 * @returns {Date}
 */
function diffHoursFromDate(date, hours){
	if (date === undefined || date === null || hours === undefined || hours === null){
		return new Date();
	}
	let base = new Date(date);
	let resultInMs = base.getTime() - hours * 60 * 60 * 1000;
	return new Date(resultInMs);
}

/**
 * Compares two dates and returns whether they are the same or not
 * @param dateLastUpdated
 * @param currentDate
 * @returns {boolean}
 */
function compareDates(dateLastUpdated, currentDate){
	return dateLastUpdated === currentDate;
}