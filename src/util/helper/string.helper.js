'use strict';

module.exports = {
	capitalize: capitalize,
	replacer:replacer,
	isPotentialInjection:isPotentialInjection
};

/**
 * Capitalize given strings
 * @param item
 * @returns {string}
 */
function capitalize(item) {
	if (item === undefined || item === 'undefined' || item === null || item.length < 1)
		return '';

	if (!isNaN(item))
		item = item+'';

	if (Array.isArray(item) || !(typeof item === 'string'))
		return '';

	let result = '';
	let arr = item.toLocaleLowerCase().split(' ');
	for(let i = 0; i < arr.length; i++){
		result = result + ' ' + arr[i].charAt(0).toLocaleUpperCase() + arr[i].slice(1);
	}

	return result.trim();
}

/**
 * Replace some stuff and normalize the items
 * @param item
 * @returns {*}
 */
function replacer(item){
	if (item === null || item === undefined || item.length < 1 || item === 'undefined'){
		return '';
	}

	while (item.includes('\'') || item.includes(';')){
		item = item + '';
		item = item.replace('\'', '`');
		item = item.replace(';', ',');
	}
	return item;
}

/**
 * proof if the input is potential an Injection
 * @param param
 * @returns {boolean}
 */
function isPotentialInjection(param){
	if (Array.isArray(param) || param === undefined || param === null)
		return true;
	let proof = param + '';
	return proof.includes('%') || (proof.includes('\\')) ||(proof.includes('#')) || (proof.includes('&')) || (proof.includes('$')) || proof.length > 42;
}