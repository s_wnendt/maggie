/* eslint-disable no-console */
'use strict';

const Transport = require('winston-transport');
const mariadb = require('mariadb');



module.exports = class YourCustomTransport extends Transport {
	constructor(opts) {
		super(opts);
	}

	async log(info, callback) {

		let pool = await mariadb.createPool({
			user: 'root',
			password: 'q1w2e3r4t5',
			host: 'dbserver.service.lokal',
			database: 'maggie_prod',
			connectionLimit: 100
		});
		let conn;
		try {
			conn = await pool.getConnection();
			let values = [info.level, info.shortDescription, info.longDescription, new Date()];
			let stmt = 'insert into maggie_prod.logs (type, shortdescription, description, timestamp) VALUES\n' +
				'  (?, ?, ?, ?)';
			await conn.query(stmt, values);
		} catch(err){
			throw err;
		} finally {
			if (conn){
				await conn.end();
				await pool.end();
			}
		}
		callback();
	}
};