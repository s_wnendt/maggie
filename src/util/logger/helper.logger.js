'use strict';

module.export = {
	importSQL: buildImportSQLScript
};

/**
 * Build the interface to the Log-Database
 * @param log
 * @returns {string|string}
 * @deprecated
 */
function buildImportSQLScript(log){
	let cmd = ''
	cmd = 'insert Logs (Type, ShortDescription, Description, TimeStamp) VALUES\n' +
		'  (\'info\', \'test\', \'test\', SYSDATETIMEOFFSET())';
	return cmd;
}