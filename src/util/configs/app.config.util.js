'use strict';

module.exports = {
	DB_HOST: 'dbserver.service.lokal',
	DB: 'maggie_prod',
	USER: 'root',
	PW: 'q1w2e3r4t5',
	SYNC: false,
	SYNC_FORCE: false,
	OPTIONS: {
		host: 'dbserver.service.lokal',
		dialect: 'mariadb',
		logging: false,
		pool: {
			max: 5,
			min: 1,
			acquire: 10000,
			idle: 10000,
			evict: 10000,
			handleDisconnects: true
		},
	},
};