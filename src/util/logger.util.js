'use strict';

module.exports = {
	log: log,
	warning: warning,
	err:err
};

let winston = require('winston');
let Transport = require('../util/logger/transport.logger');

const logger = winston.createLogger({
	format: winston.format.json(),
	transports: [
		new winston.transports.Console(),
		new Transport
	]
});

/**
 * Simple Log
 * @param short
 * @param long
 */
function log(short, long) {
	logger.log({
		level: 'info',
		shortDescription: short,
		longDescription: long
	});
}

/**
 * warn-log
 * @param short
 * @param long
 */
function warning(short, long) {
	logger.log({
		level: 'warn',
		shortDescription: short,
		longDescription: long
	});
}

/**
 * error-log
 * @param short
 * @param long
 */
function err(short, long) {
	logger.log({
		level: 'error',
		shortDescription: short,
		longDescription: long
	});
}