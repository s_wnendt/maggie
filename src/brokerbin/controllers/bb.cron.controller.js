'use strict';

module.exports = {
	controlAction: controlAction
};

/**
 * Dispatcher for POST-Request
 * @param req
 * @param res
 */
function controlAction(req, res){
	let service = require('../services/bb.cron.service');
	if (req.body.action === null) {
		res.status(400).send('missing Action');
	}
	let action = req.body.action;
	if (action === 'start'){
		service.startCron();
		res.status(200).send(true);
	} else if (action === 'stop'){
		service.stopCron();
		res.status(200).send(true);
	} else if (action === 'change'){
		service.changeTime();
		res.status(200).send(true);
	} else if (action === 'getState') {
		let state = service.getState();
		res.status(200).send(JSON.stringify(state));
	} else {
		res.status(403).send('invalid Action');
	}
}