'use strict';

let permission = require('../../user/services/user.permission.service');
let userRole = require('../../user/options/user.userrole.options');

module.exports = {
	controlAction: controlAction
};

/***
 * Dispatcher for POST-Requests
 * @param req
 * @param res
 */
async function controlAction(req, res){

	let auth = req.get('authorization');
	if (!auth) {
		return res.status(403).send([]);
	}

	if (!(await permission.hasPermission(auth, userRole.user))){
		return res.status(403).json([]);
	}

	if (req.body.action === null || req.body.action === undefined) {
		res.status(400).send('missing Action');
		return;
	}

	if (req.body.search === null || req.body.search === undefined) {
		res.status(400).send('missing Name');
		return;
	}

	let action = req.body.action;
	if (action === 'search'){
		let service = require('../services/bb.mpn.service');
		let element = await service.searchMpn(req.body.search, req.body.filter);
		res.send(element);
	} else {
		res.status(403).send('invalid Action');
	}
}