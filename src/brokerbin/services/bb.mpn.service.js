/* eslint-disable no-mixed-spaces-and-tabs */
'use strict';

let logger = require('../../util/logger.util');
let TAG = 'bb.mpn.service';
let numeral = require('numeral');
let ArrayList = require('arraylist');
let stringUtil = require('../../util/helper/string.helper');
let model = require('../model/bb.mpn.model');
let soap = require('soap');
let searchResultRepo = require('../../public/respositories/public.searchresults.repository');
let dateHelper = require('../../util/helper/date.helper');
let constants = require('../../util/statics/constants.statics');
let continentStats = require('../../util/statics/iso3166region.statics');
const exchanges = require('../../public/services/public.exchange.service');

/***
 * Service Interface
 * @type {{searchMpn: function(*, *), proofOfIncludes: function(*, *): boolean}}
 */
module.exports = {
	searchMpn: searchMpn,
	proofOfIncludes:proofOfIncludes
};

/***
 * BB-Login-Configuration
 * @type {{name: string, loginPass: string, loginSao: string, publicKey: string}}
 */
let config = {
	name: 'decteam',
	//loginPass: 'Changeme1',
	loginPass: 'ST@eW@eTH*',
	loginSao: 'vE8aF5M92oRduas5',
	publicKey: 'yTtG1EtON5uFIeuS'
};

/***
 *
 * @param searchParam
 * @param filter
 * @param isChached
 * @returns {Promise<*>}
 */
async function searchMpn(searchParam, filter, isChached) {
	model.init();

	if (searchParam.length < 2) {
		return model.getJSON();
	}
	if (isChached){
		await fillFromCache(model, searchParam, filter);
		return model.getJSON();
	}

	logger.log(TAG, 'searched for ' + searchParam);
	// let userBlow = getKey(config.publicKey, config.name);
	// logger.log('userBlow', userBlow);

	// let paaBlow = getKey(config.loginSao, config.loginPass);
	// logger.log('paaBlow', paaBlow);

	const url = 'http://soap.brokerbin.com/brokerbin_search/search.wsdl';


	// Wir haben am 11.11.2021 von Bob ein neues Passwort per eMail bekommen.
	// let args = {reqUsername: "KshG19MTRzD5YGdMdQ+1Ag==" , reqPassword: "5VidioDjk8E="};
	/**
	 * Die Funktion 'getKey()' scheint zu funktionieren aber unsere Daten in 'config' scheinen nicht zu stimmen,
	 * so dass wir das Passwort nicht selbs generieren können.
	 * Die Werte in den nächsten Zeilen sind die Ergebnisse aus der Funtion getKey().
	 */

	// let args = {reqUsername: "OjPCBneMFR0=" , reqPassword: "oX7CoFuj2fjeUjlX76dTOQ=="};
	// let args = {reqUsername: userBlow , reqPassword: paaBlow};

	/**
	 * Am 18.11.2021 haben wir von Bob einen neuen Account bekommen.
	 * Der soll nicht mehr alle 6 Monate ablaufen.
	 * reqUsername: iG5bagBy97czKgPEbpAIQw==
	 * reqPassword: kCvks/g822IB9HFX4FmpLQ==
	 *
	 * */
	let args = {reqUsername: 'iG5bagBy97czKgPEbpAIQw==' , reqPassword: 'kCvks/g822IB9HFX4FmpLQ=='};

	return new Promise(function (resolve, reject) {
		soap.createClient(url, function (err, client) {
			if (err != null) {
				logger.err('BB-1', err);
				reject({});
			}
			// logger.err('args', args);
			client.Authenticate(args, function(err, result) {
				if (err != null){
					logger.err('BB-2', err);
					reject({});
					return;
				}

				if (result === undefined ||result === null)
					reject();

				let auth = result['resSession']['$value'];
				resolve(auth);
// logger.err('BB auth', auth);

			});
		});
	}).then((auth) => {
		/** 
		 * Diese Abfrage habe ich erweitert und so umgestellt, 
		 * dass uns demnächst mögliche Authenticationsfehlschläge besser gemeldet werden.
		 */
		if (
			auth === undefined 
			|| auth === null
			|| auth === 'Failed Authentication'
			|| auth === 'Improper Credentials'
		){
                    	logger.err(TAG, 'Authentication fehlgeschlagen bei BB: ' + auth);
			if (auth === 'Failed Authentication') {
				logger.err(TAG, 'Wahrscheinlich stimmt der "reqUsername" nicht.');
			} else if (auth === 'Improper Credentials') {
				logger.err(TAG, 'Wahrscheinlich stimmt das "reqPassword" nicht.');
			} else {
				logger.err(TAG, 'Unbekannter Authenticationsfehler aufgetreten.');
			}

                    	return {};
		}

		let bbSearchParam = {
			search: searchParam,
			opts: {
				uid: auth,
				search_type: 'partkey',
				sort_by: 'price',
				sort_order: 'ASC',
				offset: 0,
				max_resultset: 200,
				omit_all: true,
				result_type: 'arr'
			}
		};
		let done = false;
// logger.err('bbSearchParam', bbSearchParam);

		return new Promise(function (resolve, reject){
			const url = 'http://soap.brokerbin.com/brokerbin_search/search.wsdl';
			soap.createClient(url, function (err, client) {
				if (err){
					logger.err(TAG, 'Api Abfrage fehlgeschlagen.');
				    reject(err);
				}
				// logger.err(TAG, 'Api Abfrage ok');
				let error = false;
				resolve(doBBRequest(done, bbSearchParam, client, error, model, filter));
			});
		}).then(() => {
			return model.getJSON();
		});

	}).catch(() => {
		logger.err(TAG, 'error');
		return model.getJSON();
	}).then(t => {
		return t;
	});
}

/**
 *
 * @param model
 * @param search
 * @param filter
 * @returns {Promise<*>}
 */
async function fillFromCache(model, search, filter){
	let list = await searchResultRepo.getAllPerSearchParam(search, dateHelper.diffHoursFromDate(new Date(), constants.CACHING_TIME_IN_HOURS), 'BB');
	for (let i = 0; i < list.length; i++){
		let g = list[i].dataValues;
		if (isFiltered(g, filter, search)) {
			model.push(validate(g));
		}
	}
	await searchResultRepo.close;
	return model;
}

/**
 * Manage the Requests to the BB-SOAP-API
 * @param done
 * @param bbSearchParam
 * @param client
 * @param error
 * @param model
 * @param filter
 * @returns {Promise<*>}
 */
async function doBBRequest(done, bbSearchParam, client, error, model, filter){
	if (error){
		let model = require('../model/bb.mpn.model');
		return model.getJSON();
	}
	if (done || model.count() > 1000){
		logger.log(TAG, 'founded: ' + model.count() + ' for ' + bbSearchParam.search);
		return model.getJSON();
	}
	return new Promise(function (resolve, reject){
		client.Search(bbSearchParam, async function(err, result) {
			let s;
// logger.err(TAG, '---BB result      :' + result.resParam);
// console.log(result.resParam.item);
			try {
				// logger.log('API-result:', result.resParam);
				s = result.resParam.item.value.item.item.value.item;

			} catch (err) {
				error = true;
				// console.log(err);
				// logger.err(TAG, '---client:      :' + client);

				logger.warning(TAG, 'not found for ' + bbSearchParam.search);
				reject(model);
			}
			if (!error) {
				bbSearchParam.opts.offset = bbSearchParam.opts.offset + bbSearchParam.opts.max_resultset;
				if(s.item !== undefined){
					let arr = [];
					arr.push(s);
					s = arr;
				}
				if (s.length < bbSearchParam.opts.max_resultset) {
					done = true;
				}

				let currencyRate = await exchanges.getExchangeRate('USD');

				for (let i = 0; i < s.length; i++) {
					let g = {};
					g.company = s[i].item[0].value['$value'].toString();
					g.country = s[i].item[1].value['$value'] + '';
					g.part = s[i].item[2].value['$value'] + '';
					g.mfg = s[i].item[3].value['$value'] + '';

					g.mfg =  stringUtil.capitalize(g.mfg.toLowerCase());

					g.cond = s[i].item[4].value['$value'] + '';
					if (g.cond === 'OEMREF')
						g.cond = 'OEMR';
					g.priceDoll = s[i].item[5].value['$value'] + '';

					if (g.priceDoll !== 'CALL')
						g.priceEur = (g.priceDoll / currencyRate) + '';
					else {
						g.priceEur = 'CALL';
					}
					g.qtyState = '';
					g.qty = s[i].item[6].value['$value'] + '';
					g.age = s[i].item[7].value['$value'] + '';
					g.description = s[i].item[8].value['$value'] + '';
					g.channel = 'BB';
					g.conti = continentStats[g.country];
					await searchResultRepo.addElement(validate(g));
					await searchResultRepo.close;
					if (isFiltered(g, filter, bbSearchParam.search)) {
						model.push(validate(g));
					}
				}
			} 
			resolve(doBBRequest(done, bbSearchParam, client, error, model, filter));
		});
	}).then(t => {
		return t;
	}).catch((model) => {
		return model.getJSON();
	});
}

/***
 * Filtered the searchstring against an Meta-Filter
 * @param item
 * @param meta
 * @param search
 * @returns {boolean}
 */
function isFiltered(item, meta, search) {
	if (meta === undefined || meta === null)
		return true;

	if (isBlacklisted(meta.blackCompany, item.company))
		return false;

	if (meta.filter_tree !== undefined)
		if(!filteredByTree(meta.filter_tree, item))
			return false;

	let result = true;

	if (!proofOfIncludes(item.company, meta.company))
		result = false;
	if (!proofOfIncludes(item.conti, meta.conti))
		result = false;
	if (!proofOfIncludes(item.country, meta.country))
		result = false;
	if (!proofOfIncludes(item.part, meta.part))
		result = false;
	if (!proofOfIncludes(item.mfg, meta.mfg))
		result = false;
	if (!proofOfIncludes(item.cond, meta.cond))
		result = false;
	if (!proofOfIncludes(item.description, meta.description))
		result = false;
	if (meta.cyber_off === 'true' || meta.cyber_off === true){
		if (proofOfIncludes(item.company, 'cybertrading'))
			result = false;
	}
	if (meta.exact_match === 'true' || meta.exact_match === true){
		if (!(item.part.toLowerCase() === search.toLowerCase()))
			result = false;
	}
	return result;
}

/***
 * Filtergeraffel
 * @param filter
 * @param item
 * @returns {boolean}
 */
function filteredByTree(filter, item){
	let logicList_condition = new ArrayList();
	let logicList_MFG = new ArrayList();
	let logicList_conti = new ArrayList();

	for (let i= 0; i < filter.length; i++) {
		if (filter[i]['parent'] === undefined){
			continue;
		}
		if (filter[i]['parent'] === 'Condition'){
			logicList_condition.add(filter[i]['child']);
		} if (filter[i]['parent'] === 'MFG'){
			logicList_MFG.add(filter[i]['child']);
		} if (filter[i]['parent'] === 'Region') {
			logicList_conti.add(filter[i]['child']);
		}
	}
	let cond = true;
	if(logicList_condition.size() > 0){
		cond = logicList_condition.contains(item.cond);
	}
	let mfg = true;
	if(logicList_MFG.size() > 0){
		mfg = logicList_MFG.contains(item.mfg);
	}
	let conti = true;
	if(logicList_conti.size() > 0){
		conti = logicList_conti.contains(item.conti);
	}

	return cond && mfg && conti;
}

/**
 * Proofs the if a given String includes an String
 * @param item
 * @param comparable
 * @returns {boolean}
 */
function proofOfIncludes(item, comparable){
	if (comparable === undefined || comparable === null){
		return true;
	}
	if (item === undefined || item === null){
		return false;
	}
	return item.toLowerCase().includes(comparable.toLowerCase());
}

/***
 * calculates the key for BB
 * @param key
 * @param value
 * @returns {string}
 */
function getKey(key, value){
	let Blowfish = require('javascript-blowfish').Blowfish;
	let bf = new Blowfish(key);
	return bf.base64Encode(bf.encrypt(value));
}

/***
 * Proof of Blacklisting
 * @param list
 * @param item
 * @returns {*}
 */
function isBlacklisted(list, item){
	if (list === undefined || item === undefined)
		return false;
	return (list.includes(item));
}

/***
 * Simple Validation of the BrokerBin given Object
 * @param obj
 * @returns {*}
 */
function validate(obj){
	obj.company = 		stringUtil.replacer(obj.company);
	obj.country = 		stringUtil.replacer(obj.country);
	obj.part = 			stringUtil.replacer(obj.part);
	obj.mfg = 			stringUtil.replacer(obj.mfg);
	obj.cond = 			stringUtil.replacer(obj.cond);
	obj.priceDoll = 	stringUtil.replacer(obj.priceDoll);
	obj.priceEur = 		stringUtil.replacer(obj.priceEur);
	obj.qty = 			stringUtil.replacer(obj.qty);
	obj.age = 			stringUtil.replacer(obj.age);
	obj.description = 	stringUtil.replacer(obj.description);

	if (obj.priceDoll !== 'CALL')
		obj.priceDoll = numeral(obj.priceDoll).format('0.00');
	if (obj.priceEur !== 'CALL')
		obj.priceEur = numeral(obj.priceEur).format('0.00');

	return obj;
}
