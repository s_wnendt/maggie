'use strict';

let logger = require('../../util/logger.util');

module.exports = {
	startCron: startCron,
	stopCron: stopCron,
	changeTime: changeTime,
	getState: getState
};

let cron;
let task = null;

let stateArray = {};

function init(){
	if (task !== null )
		return;
	cron = require('node-cron');

	stateArray.name = 'BrokerBin SearchTask';
	stateArray.state = 'init';

	task = cron.schedule('* * * * *', function(){
		buildTask();
	}, false);
}

function startCron() {
	init();
	buildTask();
	task.start();
	stateArray.state = 'started';
}

function stopCron() {
	init();
	task.stop();
	stateArray.state = 'stopped';
}

function changeTime(cronString) {
	if (!cron.validate(cronString))
		return;
	init();
	stopCron();
	task = cron.schedule(cronString, function(){
		buildTask();
	}, false);
}

function getState() {
	init();
	return stateArray;
}

function buildTask(){
	logger.log('Cron','Search Task');
	let todo = require('../tasks/search.task');
	todo.run();
}