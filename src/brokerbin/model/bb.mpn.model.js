'use strict';

let logger = require('../../util/logger.util');
let TAG = 'bb.mpn.model';

let ArrayList = require('arraylist');

/**
 * Model-Interface
 * @type {{init: function(), push: push, getJSON: function(): string, count: function(): number}}
 */
module.exports = {
	init: init,
	push: push,
	getJSON: getJSON,
	count:count,
	getCachedModel: getCachedModel,
};

let data = [];
let sum = {};

/**
 * Initiate the model.
 */
function init() {
	data = [];
	sum = {};
	sum.condition = new ArrayList();
	sum.conditionCount = [];

	sum.manufacturer = new ArrayList();
	sum.manufacturerCount = [];

	sum.country = new ArrayList();
	sum.countryCount = [];

	sum.region = new ArrayList();
	sum.regionCount = [];

	sum.qtyState = new ArrayList();
	sum.qtyStateCount = [];
}

/**
 * push a BB-Item to the Model.
 * @param item
 */
function push(item) {

	buildDataSet(sum.condition, sum.conditionCount, item.cond);
	buildDataSet(sum.manufacturer, sum.manufacturerCount, item.mfg);
	buildDataSet(sum.country, sum.countryCount, item.country);
	let iso3166region = require('../../util/statics/iso3166region.statics');
	buildDataSet(sum.region, sum.regionCount, iso3166region[item.country]);
	buildDataSet(sum.qtyState, sum.qtyStateCount, item.qtyState);

	data.push(item);
}

/**
 * Build the dataset for Table-Model
 * @param list
 * @param countList
 * @param item
 */
function buildDataSet(list, countList, item) {
	if (item === undefined || item.length < 1)
		return;
	if (!list.contains(item)){
		countList.push({key: item, value: 1});
		list.add(item);
	} else {
		for (let i1 = 0; i1 < countList.length; i1 ++){
			if (countList[i1].key === item){
				countList[i1].value = countList[i1].value + 1;
			}
		}
	}
}

/**
 * transform the model into JSON-Format
 * @returns {string}
 */
function getJSON() {
	let model = {};
	model.data = data;
	model.sum = sum;
	return JSON.stringify(model);
}

/**
 * simple Count of the Elements
 * @returns {number}
 */
function count(){
	return data.length;
}

function getCachedModel(){
	let model = require('../../public/respositories/public.searchresults.repository');
}