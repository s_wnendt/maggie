'use strict';

let logger = require('../../util/logger.util');
let configMaggie = require('../../util/db/config.maggie.db');
let TAG = 'search.task:BrokerBin';
const sql = require('mssql');

module.exports = {
	run:run
};

let config = {
	name: 'decteam',
	loginPass: 'ST@eW@eTH*',
	loginSao: 'vE8aF5M92oRduas5',
	publicKey: 'yTtG1EtON5uFIeuS'
};

// TODO Refactoring
function run() {
	logger.log(TAG,'BB-Run is going');
	let userBlow = getKey(config.publicKey, config.name);
	let paaBlow = getKey(config.loginSao, config.loginPass);
	let soap = require('soap');
	let url = 'http://soap.brokerbin.com/brokerbin_search/search.wsdl';
	let args = {reqUsername: userBlow, reqPassword: paaBlow};
	soap.createClient(url, function(err, client) {
		client.Authenticate(args, function(err, result) {
			if (err != null){
				logger.err('BB', err);
				return;
			}
			let auth = result['resSession']['$value'];
			if (auth !== null ){
				const pool = new sql.ConnectionPool(configMaggie, err => {
					if (err != null){
						logger.log(TAG, err.message);
						return;
					}
					pool.request().query(getElementsforBBSearch(4), (err, result) => {
						if (err != null) {
							logger.log(TAG, err);
							return;
						}
						if (result === null ) {
							return;
						}
						for (let o = 0; o < result.recordset.length; o++) {
							let args2 = {
								search: result.recordset[o]['MPN'],
								opts: {
									uid: auth,
									search_type: 'partkey',
									sort_by: 'partsno',
									sort_order: 'ASC',
									offset: 0,
									max_resultset: 200,
									omit_all: true,
									result_type: 'arr'
								}
							};
							MarkAsDone(args2.search);
							let model = [];
							let url = 'http://soap.brokerbin.com/brokerbin_search/search.wsdl';
							soap.createClient(url, function (err, client) {
								client.Search(args2, function (err, result) {
									let array;
									try {
										array = result.resParam.item.value.item.item.value.item;
									} catch (err){
										logger.log(TAG, 'API Error for ' + args2.search);
										return;
									}

									for (let i = 0; i < array.length; i++) {
										let g = {};
										g.company = array[i].item[0].value['$value'].toString();
										g.country = array[i].item[1].value['$value']+ '';
										g.part = array[i].item[2].value['$value']+ '';
										g.mfg = array[i].item[3].value['$value']+ '';
										g.cond = array[i].item[4].value['$value']+ '';
										g.price = array[i].item[5].value['$value']+ '';
										g.qty = array[i].item[6].value['$value']+ '';
										g.age = array[i].item[7].value['$value']+ '';
										g.describtion = array[i].item[8].value['$value']+ '';
										g.clei = array[i].item[9].value['$value']+ '';
										g.status = array[i].item[10].value['$value']+ '';

										validate(g);

										model.push(validate(g));
									}
									if (model.length < 1)
										return;
									const pool1 = new sql.ConnectionPool(configMaggie, err => {
										if (err != null) {
											logger.log(TAG, err.message);
										}
										pool1.request()
											.query(importBBDataSets(model), (err) => {
												if (err != null) {
													logger.log(TAG, err.message);
												}
											});
									});
								});
							});
						}
					});
				});
			}
		});
		logger.log(TAG, 'BB-Import done');
	});
}

function getKey(key, value){
	let Blowfish = require('javascript-blowfish').Blowfish;
	let bf = new Blowfish(key);
	return bf.base64Encode(bf.encrypt(value));
}

function getElementsforBBSearch(top){
	return `SELECT Top ${top}BD.MPN, Date from BaseData BD
			\tLEFT JOIN BB_Worklist BW ON BD.MPN = BW.MPN
			GROUP BY BD.MPN, Date
			order BY Date asc`;
}

function importBBDataSets(model){
	if (model.length === 0)
		return;
	let cmd = 'INSERT INTO BB_Data (GUID, Company, Country, Part, Mfg, Cond, Price, Qty, Age, Description, Clei, Status, Date)\n' +
		'\t\tVALUES ';
	for (let i = 0; i< model.length; i++){
		let helper = require('../../util/helper/util.helper');
		let uuid = helper.guid();
		cmd = cmd +
			` ('${uuid}',
			'${model[i].company}',
			'${model[i].country}',
			'${model[i].part}',
			'${model[i].mfg}',
			'${model[i].cond}',
			'${model[i].price}',
			'${model[i].qty}',
			'${model[i].age}',
			'${model[i].describtion}',
			'${model[i].clei}',
			'${model[i].status}', 
			SYSDATETIMEOFFSET())`;
		if (i !== model.length -1){
			cmd = cmd + ',';
		}
	}
	cmd = cmd + ' ';
	return cmd;
}

function validate(obj){
	obj.company = replacer(obj.company);
	obj.country = replacer(obj.country);
	obj.part = replacer(obj.part);
	obj.mfg = replacer(obj.mfg);
	obj.cond = replacer(obj.cond);
	obj.price = replacer(obj.price);
	obj.qty = replacer(obj.qty);
	obj.age = replacer(obj.age);
	obj.describtion = replacer(obj.describtion);
	obj.status = replacer(obj.status);

	return obj;
}

function replacer(item){
	if (item === null){
		item = '';
	}
	// TODO sehr hässlich
	while (item.includes('\'') || item.includes(';')){
		item = item + '';
		item = item.replace('\'', '`');
		item = item.replace(';', ',');
	}
	return item;
}

function MarkAsDone(obj){
	const pool1 = new sql.ConnectionPool(configMaggie, err => {
		if (err != null){
			logger.log(TAG, err.message);
			return;
		}
		pool1.request() // or: new sql.Request(pool1)
			.query(markSQL(obj), (err) => {
				if (err != null){
					logger.log(TAG, err.message);
				}
			});
	});
}

function markSQL(mpn){
	let cmd;
	cmd = `UPDATE BB_Worklist SET Date = SYSDATETIMEOFFSET() WHERE MPN='${mpn}'
		\t\tIF @@ROWCOUNT=0
    \tinsert INTO BB_Worklist (MPN, Date)
    \tVALUES ('${mpn}', SYSDATETIMEOFFSET())`;
	return cmd;
}