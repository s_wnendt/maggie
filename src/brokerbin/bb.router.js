'use strict';


let path = '/bb/';

/**
 * Routing for the controllers
 * @param app
 */
module.exports = function (app) {
	//cron-api
	let cronController = require('./controllers/bb.cron.controller');
	app.post(path+'cron/', cronController.controlAction);
	let mpnController = require('./controllers/bb.mpn.controller');
	app.post(path+'mpn/', mpnController.controlAction);
};