'use strict';
let logger = require('../../util/logger.util');
let sql = require('mssql');
let horstConfig = require('../../util/db/config.horst.prod.db');
let TAG = 'mpn.task:get:mpn';

module.exports ={
	run:run
};

/**
 *
 * @returns {Promise<void>}
 */
async function run(){
	sql.close();
	let pool = await sql.connect(horstConfig);
	let productRepo = require('../../public/respositories/public.product.repository');

	let result1 = await pool.request().query(getSQL());

	let container = result1.recordsets['0'];


	for(let i= 0; i < container.length; i++){
		await productRepo.saveProduct(
			container[i]['ManufacturerProductNumber'],
			container[i]['ReferenceCode'],
			container[i]['VariationGuid']);
	}
	logger.log(TAG,'FullImport Done');
}

/**
 * simple static SQL
 */
function getSQL() {
	return 'Select ManufacturerProductNumber, ReferenceCode, pv.VariationGuid from BaseData.ProductVariation as pv\n  join BaseData.Product as p on p.Id = pv.ProductId\nwhere 1=1\n  and DateDeleted is NULL';
}