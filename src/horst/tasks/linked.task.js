'use strict';
let logger = require('../../util/logger.util');
let sql = require('mssql');
let horstConfig = require('../../util/db/config.horst.prod.db');
let TAG = 'mpn.task:get:linked';

module.exports ={
	run:run
};

/**
 *
 * @returns {Promise<void>}
 */
async function run(){
	sql.close();
	let pool = await sql.connect(horstConfig);
	let linkedRepo = require('../../public/respositories/public.linked.repository');
	linkedRepo.dropTable();
	let result1 = await pool.request().query(getSQL());

	let container = result1.recordsets['0'];


	for(let i= 0; i < container.length; i++){
		await linkedRepo.addLinked(
			container[i].parent.toLowerCase(),
			container[i].child.toLowerCase()
		);
	}
	logger.log(TAG,'Linked-Import Done');
}

/**
 * simple static SQL
 */
function getSQL() {
	return 'Select\n' +
		'  pc.ManufacturerProductNumber as \'child\',\n' +
		'  pp.ManufacturerProductNumber as \'parent\'\n' +
		'\n' +
		'from BaseData.ProductVariationMapping as pvm\n' +
		'  left join BaseData.ProductVariation as child on pvm.ChildVariationGuid = child.VariationGuid\n' +
		'    join BaseData.Product as pc on pc.Id  = child.ProductId\n' +
		'  left join BaseData.ProductVariation as parent on pvm.ParentVariationGuid = parent.VariationGuid\n' +
		'    join BaseData.Product as pp on pp.Id  = parent.ProductId\n' +
		'where 1=1\n' +
		'  group by pc.ManufacturerProductNumber, pp.ManufacturerProductNumber';
}