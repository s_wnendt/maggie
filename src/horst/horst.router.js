'use strict';


let path = '/horst/';

module.exports = function (app) {
	//cron-api
	let cronController = require('./controllers/horst.cron.controller');
	app.post(path+'cron/', cronController.controlAction);
	let exchangeController = require('./controllers/horst.exchange.controller');
	app.post(path+'exchange/', exchangeController.exchangeAction);
	let productController = require('./controllers/horst.product.controller');
	app.post(path+'product', productController.addProductAction);
};