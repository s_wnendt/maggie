'use strict';

let logger = require('../../util/logger.util');
let TAG = 'horst.product.service';

module.exports = {
	addProductToHorst:addProductToHorstWorkflow,
	existsInHorst:productExistsInHorst
};

/**
 * add the article to the workflow of horst
 * @param model
 * @returns {Promise<void>}
 */
async function addProductToHorstWorkflow(model){
	if (isValid(model)){
		let repo = require('../repositories/horst.newproduct.repository');
		let result = await repo.addNewProduct(model);
		if (result === undefined ||result === null)
			logger.err(TAG, JSON.stringify(model));
	}
}

/**
 * proof if the article is valid
 * @param model
 * @returns {boolean}
 */
function isValid(model){
	let valid = true;
	if (model['manufacturer'] === undefined || model['manufacturer'].length < 1)
		valid = false;
	if (model['partnumber'] === undefined || model['partnumber'].length < 1)
		valid = false;
	if (model['listprice'] === undefined)
		valid = false;
	if (model['description'] === undefined)
		valid = false;
	return valid;
}

/**
 * Exist the article in the stocklist
 * @param mpn
 * @param manu
 * @returns {Promise<number>}
 */
async function productExistsInHorst(mpn, manu){
	if (mpn === undefined || mpn === null) {
		return -1;
	}
	if (manu === undefined || manu === null){
		return -1;
	}
	let serverResult = await getProductAPI(mpn);
	for (let i = 0; i < serverResult['Items'].length; i++){
		let item = serverResult['Items'][i];
		if (item['ManufacturerProductNumber'].toLocaleLowerCase() === mpn.toLocaleLowerCase()){
			if (item['ManufacturerLabel'].toLocaleLowerCase() === manu.toLocaleLowerCase()){
				return 1;
			}
		}
	}
	return 0;
}

/**
 * get the information of an product per API
 * @param mpn
 * @returns {Promise<R>}
 */
async function getProductAPI(mpn) {
	let rp = require('request-promise');
	let options = {
		url: 'https://cybertrading-prod.azurewebsites.net/api/Products?userId=113&page=1&queryTerm='+mpn,
		// ToDo
		headers: {
			'User-Agent': 'Fiddler',
			'Accept': 'application/json',
			'token': '1dM6hjnt510K2jsqRnHe',
			'login': 'test',
			'client-version': '1.1.2.4',
			'culture': 'de-DE',
			'utc-offset-seconds': '60',
			'Ocp-Apim-Trace': 'true',
			'Ocp-Apim-Subscription-Key': '1dM6hjnt510K2jsqRnHe',
			'Host': 'cybertrading-prod.azurewebsites.net'
		}
	};

	return await rp(options)
		.then(function (repos) {
			let obj = JSON.parse(repos);
			return obj;
		})
		.catch(function (err) {
			return err;
		});
}