'use strict';

let logger = require('../../util/logger.util');

module.exports = {
	startCron: startCron,
	stopCron: stopCron,
	changeTime: changeTime,
	getState: getState
};

let cron;
let task = null;

function init(){
	if (task !== null )
		return;
	cron = require('node-cron');

	task = cron.schedule('0 0 */2 * * *', function(){
		buildTask();
	}, false);
}

function startCron(){
	init();
	buildTask();
	task.start();
}

function stopCron() {
	init();
	task.stop();
}

function changeTime(cronString) {
	if (!cron.validate(cronString))
		return;
	init();
	stopCron();
	task = cron.schedule(cronString, function(){
		buildTask();
	}, false);
}

function getState() {
	init();
	return task;
}

function buildTask(){
	logger.log('Cron','Get new MPN\'s');
	let todo = require('../tasks/mpn.task');
	todo.run();
}