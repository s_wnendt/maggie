'use strict';

let sql = require('mssql');
let horstConfig = require('../../util/db/config.horst.prod.db');
let ArrayList = require('arraylist');
let logger = require('../../util/logger.util');

module.exports = {
	getHistoryData:getHistoryData,
	getLogicErrors:getLogicErrors,
	getProductVariation: getProductVariation
};

/**
 *
 * @param refCode
 * @returns {Promise<R>}
 */
async function getProductVariationFromHorstOfRefCode(refCode) {
	let rp = require('request-promise');
	let options = {
		url: 'https://cybertrading-prod.azurewebsites.net/api/ProductVariations/GetByRef?refCode='+refCode,
		// ToDo
		headers: {
			'User-Agent': 'Fiddler',
			'Accept': 'application/json',
			'token': '1dM6hjnt510K2jsqRnHe',
			'login': 'test',
			'client-version': '1.1.2.4',
			'culture': 'de-DE',
			'utc-offset-seconds': '60',
			'Ocp-Apim-Trace': 'true',
			'Ocp-Apim-Subscription-Key': '1dM6hjnt510K2jsqRnHe',
			'Host': 'cybertrading-prod.azurewebsites.net'
		}
	};

	return await rp(options)
		.then(function (repos) {
			let obj = JSON.parse(repos);
			return obj;
		})
		.catch(function (err) {
			return err;
		});
}

/**
 *
 * @param model
 * @returns {Promise<string>}
 */
async function getHistoryData(model){
	let item = await getProductVariationFromHorstOfRefCode(model);
	sql.close();
	if(item.Value === null || item.Value === undefined){
		return JSON.stringify([]);
	}
	let pool = await sql.connect(horstConfig);
	let result1 = await pool.request().query(getSql(model));
	let container = result1.recordsets;
	pool.close();
	let obj = await buildDataSet4HistoryData(container, item);

	return JSON.stringify(obj);
}

/**
 *
 * @param container
 * @param pub
 * @returns {Promise<*|Array>}
 */
async function buildDataSet4HistoryData(container, pub){
	let result = new ArrayList();
	for(let i = 0; i < container['0'].length; i++){
		result.add({
			version: container['0'][i]['VersionNumber'],
			user: container['0'][i]['Displayname'],
			timestamp: container['0'][i]['DateVersion'],
			bbPrice: container['0'][i]['PurchasePrice'],
			mpPrice: container['0'][i]['MarketplacePrice'],
			marge: container['0'][i]['Marge'],
			actualQuantity: container['0'][i]['ActualQuantity'],
			bbQuantity: container['0'][i]['Quantity'],
			marketPlaceQuantity: container['0'][i]['MarketplaceQuantity'],
		});
	}
	result.add({
		version: pub.Value['VersionNumber'],
		user: pub.Value['CheckerText'],
		timestamp: pub.Value['VersionDate'],
		bbPrice: pub.Value['PurchasePrice'],
		mpPrice: pub.Value['BaseMarketplacePrice'],
		marge: pub.Value['Marge'],
		actualQuantity: pub.Value['ActualQuantity'],
		bbQuantity: pub.Value['Quantity'],
		marketPlaceQuantity: pub.Value['BaseMarketplaceQuantity'],
	});

	let ret = result.toArray();
	ret.sort(sortHistorie);

	return ret;
}

/**
 * @deprecated
 * get logicErrors
 * @returns {string}
 */
function getLogicErrors(){
	let result = [];

	let item1 = {
		refCode: 'alrik-ref',
		user: 'AS',
		timestamp: new Date(),
		reason: 'kein Preis'
	};
	let item2 = {
		refCode: 'alrik-ref',
		user: 'AS',
		timestamp: new Date(),
		reason: 'kein Preis'
	};
	result.push(item1);
	result.push(item2);

	return JSON.stringify(result);
}

/**
 * get Sql for all Productvariations
 * @param item
 * @returns {string|*}
 */
function getSql(item) {
	let cmd;
	cmd = 'select VersionNumber, u.Displayname, p.CheckedOn, p.PurchasePrice, Marge, Quantity, ActualQuantity, Notes, BundleInformation, SalesInformation, Comment, MarketplacePrice, MarketplaceQuantity, DateDeleted, DateVersion  from BaseData.ProductVariationHistory as p\n' +
		'  join UserData.[User] as u on u.Id = p.CheckerId\n' +
		'where 1=1\n' +
		'  and ReferenceCode = \''+item+'\'';
	return cmd;
}

/**
 * sort :D
 * @param a
 * @param b
 * @returns {number}
 */
function sortHistorie(a,b) {
	return b.version - a.version;
}

/**
 * get the pv from horst per mpn and condition
 * @param mpn
 * @param condition
 * @returns {undefined}
 */
async function getProductVariation(mpn, condition){
	let result;
	if (mpn === null || mpn === undefined || condition === null || condition === undefined)
		return undefined;
	let productRepo = require('../../public/respositories/public.product.repository');
	let resultSet = await productRepo.getPerMPNandCondition(mpn, condition);
	if (resultSet.length < 1 || resultSet[0].dataValues === undefined)
		return undefined;
	result = resultSet[0].dataValues;
	let item = await getProductVariationFromHorstOfGUID(result['guid_horst']);
	return item;
}

/**
 *
 * @param refCode
 * @returns {Promise<R>}
 */
async function getProductVariationFromHorstOfGUID(guid) {
	let rp = require('request-promise');
	let options = {
		url: 'https://cybertrading-prod.azurewebsites.net/api/ProductVariations/GetByGuid?guid='+guid,
		// ToDo
		headers: {
			'User-Agent': 'Fiddler',
			'Accept': 'application/json',
			'token': '1dM6hjnt510K2jsqRnHe',
			'login': 'test',
			'client-version': '1.1.2.4',
			'culture': 'de-DE',
			'utc-offset-seconds': '60',
			'Ocp-Apim-Trace': 'true',
			'Ocp-Apim-Subscription-Key': '1dM6hjnt510K2jsqRnHe',
			'Host': 'cybertrading-prod.azurewebsites.net'
		}
	};

	return await rp(options)
		.then(function (repos) {
			let obj = JSON.parse(repos);
			return obj;
		})
		.catch(function (err) {
			return err;
		});
}