'use strict';

module.exports = {
	getExchange: getExchange,
};

/**
 * gets the exchange dataset
 * @returns {Promise<T>}
 */
async function getExchange(){
	let rp = require('request-promise');
	let options = {
		url: 'https://cybertrading-prod.azurewebsites.net/api/Exchanges',
		// ToDo
		headers: {
			'User-Agent': 'Fiddler',
			'Accept': 'application/json',
			'token': '1dM6hjnt510K2jsqRnHe',
			'login': 'test',
			'client-version': '1.1.2.4',
			'culture': 'de-DE',
			'utc-offset-seconds': '60',
			'Ocp-Apim-Trace': 'true',
			'Ocp-Apim-Subscription-Key': '1dM6hjnt510K2jsqRnHe',
			'Host': 'cybertrading-prod.azurewebsites.net'
		}
	};
	return await rp(options)
		.then(function (repos) {
			let obj = JSON.parse(repos);
			return obj;
		})
		.catch(function (err) {
			return err;
		});
}