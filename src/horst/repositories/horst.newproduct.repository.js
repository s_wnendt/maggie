'use strict';

module.exports = {
	addNewProduct: addNewProduct,
	getNewProducts: getNewProducts,
	deleteNewProductsPerGUID:deleteNewProductsPerGUID,
};

let guid = require('../../util/helper/util.helper');

/**
 * Get all Products
 * @returns {Bluebird<any[]>}
 */
async function getNewProducts(){
	let result = await NewProduct.findAll().then(
		arr => {
			return arr;
		}
	);
	//await sequelize.close();
	return result;

}

/**
 * deletes an Article by an ID
 * @param deleted
 * @returns {Bluebird<number>}
 */
async function deleteNewProductsPerGUID(deleted){
	let result = await NewProduct.destroy({
		where: {guid: deleted}
	});
	//await sequelize.close();
	return result;
}

/**
 * ad a new Product into the System
 * @param model
 * @returns {Promise<any>}
 */
async function addNewProduct(model){
	let existingProduct = await NewProduct.findOne({
		where: {
			[Sequelize.Op.and] : [
				{
					manufacturer:  model['manufacturer']
				}, {
					partnumber: model['partnumber']
				}
			]
		}
	});
	if (existingProduct === null || existingProduct === undefined) {
		return await NewProduct.create({
			guid: guid.guid(),
			manufacturer: model['manufacturer'],
			partnumber: model['partnumber'],
			listprice: model['listprice'],
			description: model['description']
		});
	}
}

const config = require('../../util/configs/app.config.util');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

const NewProduct = sequelize.define('newProduct', {
	guid: {
		type: Sequelize.UUID,
		defaultValue: Sequelize.UUIDV1,
		primaryKey: true
	},
	manufacturer: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	partnumber: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	listprice: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	description: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}
});

if (config.SYNC)
	NewProduct.sync({force: config.SYNC_FORCE}).thenReturn();