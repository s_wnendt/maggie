'use strict';

module.exports = {
	addProductAction: addProductAction
};

async function addProductAction(req, res){
	let permission = require('../../user/services/user.permission.service');
	let userRole = require('../../user/options/user.userrole.options');

	if (req.body.action === null || req.body.action === undefined) {
		res.status(400).send('missing Action');
		return;
	}

	let auth = req.get('authorization');
	if (!auth) {
		return res.status(403).send([]);
	}

	if (!(await permission.hasPermission(auth, userRole.user, 'add_product'))){
		return res.status(403).json([]);
	}

	let model = {};
	let action = req.body.action;

	if (action === 'add'){
		res.sendStatus(200).send('');
		let addService = require('../services/horst.product.service');
		model.manufacturer = req.body.manufacturer;
		model.partnumber = req.body.partnumber;
		model.listprice = (req.body.listprice === undefined)? '':req.body.listprice;
		model.description = (req.body.description === undefined)? '':req.body.description;
		await addService.addProductToHorst(model);
	}  else if (action === 'exist') {
		if (req.body.mpn === undefined || req.body.mpn === null)
			res.sendStatus(200).send(-1);
		if (req.body.manu === undefined || req.body.manu === null)
			res.status(200).send(-1);
		let productService = require('../services/horst.product.service');
		let result = await productService.existsInHorst(req.body.mpn, req.body.manu);

		// workaround for Chrome-Bug

		let referal = {};
		referal.state = result;
		referal = JSON.stringify(referal);

		res.status(200).send(referal);
	}
	else {
		res.sendStatus(403).send('invalid Action');
	}
}