'use strict';

module.exports = {
	exchangeAction: exchangeAction
};

async function exchangeAction(req, res){

	let permission = require('../../user/services/user.permission.service');
	let userRole = require('../../user/options/user.userrole.options');

	let auth = req.get('authorization');
	if (!auth) {
		return res.status(403).send([]);
	}

	if (!(await permission.hasPermission(auth, userRole.user, 'exchange'))){
		return res.status(403).json([]);
	}

	let service = require('../services/horst.exchange.service');
	if (req.body.action === null || req.body.action === undefined) {
		res.status(400).send('missing Action');
		return;
	}
	let action = req.body.action;
	if (action === 'getAll'){
		let s = await (service.getExchange()
			.then(t => {
				return t;
			}));
		res.send(s);
	}  else {
		res.status(403).send('invalid Action');
	}
}