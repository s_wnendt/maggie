'use strict';

module.exports = {
	controlAction: controlAction
};

function controlAction(req, res){
	let service = require('../services/horst.cron.service');
	if (req.body.action === null) {
		res.status(400).send('missing Action');
	}
	let action = req.body.action;
	if (action === 'start'){
		service.startCron();
		res.send(true);
	} else if (action === 'stop'){
		service.stopCron();
		res.send(true);
	} else if (action === 'change'){
		service.changeTime();
		res.send(true);
	} else if (action === 'getState') {
		let state = service.getState();
		res.send(JSON.stringify(state));
	} else {
		res.status(403).send('invalid Action');
	}
}
