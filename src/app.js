'use strict';

let express = require('express');
let app = express();
let cors = require('cors');
let logger = require('../src/util/logger.util');
let bodyParser = require('body-parser');

app.use(cors({
	origin: 'http://localhost:8090'
}));

app.use(bodyParser.urlencoded({extended : true}));

app.use('/static', express.static(__dirname + '/public/views'));

require('./router')(app);

app.listen(8090, function() {
	logger.log('Start', 'Server listens on port 8090');
});
