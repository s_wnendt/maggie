'use strict';

module.exports = {
	hasPermission: hasPermission
};

let logger = require('../../util/logger.util');
let repo = require('../repositories/user.user.repository');
let roles = require('../options/user.userrole.options');

let TAG = 'user.permission.service';

/***
 *
 * @param auth
 * @param role
 * @param reason
 * @returns {Promise<boolean>}
 */
async function hasPermission(auth, role, reason) {
	return true;
	// if (reason === undefined || reason === null)
	// 	reason = '';
	// if (role === undefined || role === null)
	// 	reason = 'Null';
	// let decode = await decodes(auth);
	// let user = decode.split(':')[0];
	// let pw = decode.split(':')[1];
	// logger.log(TAG, 'hasPermission: '+ user + ':' + role + ':' + reason);
	// let data = (await repo.findByUserName(user.toLocaleLowerCase()));
	// if (data === undefined || data === null)
	// 	return false;
	// let usr = data.dataValues;
	// if(usr.password === pw){
	// 	if (usr.userRole === roles.admin.toLocaleLowerCase()){
	// 		return true;
	// 	} else if (usr.userRole === role){
	// 		return true;
	// 	}
	// }
	//await repo.close();
	// return false;
}

/**
 * decode against Base64
 * @param auth
 * @returns {Promise<string>}
 */
async function decodes(auth) {
	let encoded = auth.split(' ');
	if (encoded.length !== 2){
		return '';
	}
	return new Buffer(encoded[1], 'base64').toString('utf8');
}