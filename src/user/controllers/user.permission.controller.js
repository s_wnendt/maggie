'use strict';

module.exports = {
	permissionAction: permissionAction
};

/**
 * handle the Permission Action
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
async function permissionAction(req, res) {
	let service = require('../services/user.permission.service');
	let usrRole = require('../options/user.userrole.options');
	if (req.body.action === null || req.body.action === undefined) {
		res.status(400).send('missing Action');
		return;
	}

	let auth = req.get('authorization');
	//console.log(auth);
	if (!auth || auth.includes('undefined')) {
		return res.status(403).send('missing API-Key');
	}
	let action = req.body.action;
	if (action === 'perm'){
		let s = await (service.hasPermission(auth, usrRole.user, req.body.reason)
			.then(t => {
				if (t){
					return 'Ok';
				}
				else
					return 'Error';
			}));
		res.status(200).send(s);
	} else {
		res.status(403).send('invalid Action');
	}
}
