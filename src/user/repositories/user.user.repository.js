'use strict';

let logger = require('../../util/logger.util');
const config = require('../../util/configs/app.config.util');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

module.exports = {
	findByUserName:findByUserName,
	close:close
};

async function findByUserName(name){
	let gert = await User.findAll({
		where: {
			username: name
		}
	}).then((users, err) => {
		if (err){
			logger.err('user.user.repository', 'User '+ name +' not found');
			throw err;
		}
		return users;
	}).catch((err) =>{
		logger.err('usr-repo', 'wew');
		throw err;
		return [];
	});
	return gert[0];
}


const User = sequelize.define('user', {
	username: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	password: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	userRole: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}
});

async function close(){
	//await sequelize.close();
}

if (config.SYNC)
	User.sync({force: config.SYNC_FORCE}).thenReturn();