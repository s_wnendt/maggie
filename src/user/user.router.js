'use strict';

let path = '/user/';

module.exports = function (app) {
	let usrController = require('./controllers/user.permission.controller');
	app.post(path+'permission/', usrController.permissionAction);
};