'use strict';

let controller = require('../public/controllers/public.controller');

module.exports = function (app) {
	app.get('/v1', controller.oldAction);
	app.get('/', controller.indexAction);
	app.get('/admin', controller.adminAction);
	app.post('/get', controller.getAction);
	// general abstract data interface
	app.post('/admin/data', controller.adminDataAction);

	// Company Stuff
	app.post('/comp/', controller.companyAction);
};