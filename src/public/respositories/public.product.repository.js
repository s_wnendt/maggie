'use strict';

module.exports = {
	saveProduct:saveOrUpdateProduct,
	findPerGUID:findPerGUID,
	getPerMPNandCondition:getPerMPNandCondition,
};

const config = require('../../util/configs/app.config.util');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

/**
 * find a product per Guid
 * @param guid
 * @returns {Promise<any>}
 */
async function findPerGUID(guid){
	let gert = await Product.findAll({
		where: {
			guid_horst: guid
		}
	});
	//await sequelize.close;
	return gert[0];
}

async function getPerMPNandCondition(mpn, cond){
	const Op = Sequelize.Op;
	let gert = await Product.findAll({
		where: {
			[Op.and]: [
				sequelize.where(sequelize.fn('lower', sequelize.col('mpn')), mpn.toLowerCase()),
				sequelize.where(sequelize.fn('lower', sequelize.col('reference')), {
					[Op.like]: '%-' + cond.toLowerCase()
				})
			]
		}
	}).error(err => {
		this.err = err;
	});
	//await sequelize.close;
	return gert;
}

/**
 * safe or update a Product
 * @param mpnr
 * @param ref
 * @param horstId
 * @returns {Promise<any>}
 */
async function saveOrUpdateProduct(mpnr, ref, horstId){
	let gert = await Product.findAll({
		where: {
			guid_horst: horstId
		}
	});
	if (gert.length === 0)
		return await Product.create({
			mpn: mpnr,
			reference: ref,
			guid_horst: horstId,
		});
}

const Product = sequelize.define('product', {
	guid_horst: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: true
	},
	mpn: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	reference: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}
});

exports.close = async () => {
	//await sequelize.close;
};

if (config.SYNC)
	Product.sync({force: config.SYNC_FORCE}).thenReturn();