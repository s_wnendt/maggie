'use strict';

const config = require('../../util/configs/app.config.util');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

module.exports = {
	dropTable: dropTable,
	getLinked: getLinked,
	addLinked: addLinked
};

/***
 *
 * @param mpn
 * @param starter
 * @returns {Bluebird<any[]>}
 */
//TODO: refactoring
async function getLinked(mpn, starter){ // quite dirty stuff
	let safeReccursive = starter;
	let gert = await Linked.findAll({
		where: {
			[Sequelize.Op.or]: [
				{
					child: mpn.toLowerCase()
				},{
					parent: mpn.toLowerCase()
				}
			]
		}
	});
	let tmp = [];
	for (let i = 0; i < gert.length; i++){
		if (gert[i].dataValues.child === gert[i].dataValues.parent){
			tmp.push(gert[i]);
			continue;
		}
		if (gert[i].dataValues.child === mpn.toLowerCase() && safeReccursive < 3){
			safeReccursive++;
			tmp.push(await getLinked(gert[i].dataValues.parent, safeReccursive));
		}
	}
	if (tmp.length > 0){
		for(let j = 0; j < tmp[0].length; j++){
			gert.push(tmp[0][j]);
		}
	}
	//await sequelize.close;
	return gert;
}

/***
 *
 * @param parent
 * @param child
 */
async function addLinked(parent, child){
	let result = await Linked.create({
		child: child,
		parent: parent
	});
	//await sequelize.close();
}

/**
 * drop the given Table
 */
function dropTable(){
	Linked.sync({force: true}).thenReturn();
}

const Linked = sequelize.define('linked', {
	child: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	parent: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}
});

exports.close = async () => {
	//await sequelize.close;
};

if (config.SYNC)
	Linked.sync({force: config.SYNC_FORCE}).thenReturn();

