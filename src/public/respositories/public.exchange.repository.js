const config = require('../../util/configs/app.config.util');
const Sequelize = require('sequelize');
const dateHelper = require('../../util/helper/date.helper');
const Arraylist = require('arraylist');
const sequelize = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);


module.exports = {
	exchangeRatesAreUpToDate: exchangeRatesAreUpToDate,
	updateExchangeRate: updateExchangeRate,
	getExchangeRateFromDb: getExchangeRateFromDb,
	sync: sync
};



let exchange = sequelize.define('exchanges', {
	currency: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	rate: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}
});


/**
 * Checks whether the currency information is up-to-date or not
 * @param currency
 */
async function exchangeRatesAreUpToDate(currency){
	return await exchange.findAll({
		where: {
			'currency': currency
		}
	}).then(async res => {
		if (res === undefined || res.length < 1) {
			return false;
		}
		let item = res[0].dataValues;

		let wasUpdatedToday = dateHelper.compareDates(new Date(item['updatedAt']).toDateString(), new Date().toDateString());
		return wasUpdatedToday;
	});
}

/**
 * Add or update a search-input into DB
 * @param currency
 * @param rate
 * @returns {Promise<*>}
 */
async function updateExchangeRate(currency, rate){
	let existingItem = await exchange.findOne({
		where: {
			'currency': currency
		}
	});
	if (existingItem === null || existingItem === undefined) {
		return await exchange.create({
			'currency': currency,
			'rate': rate
		});
	}
	await exchange.update(
		{
			'rate': rate
		},
		{where :
				{
					'currency': currency
				}
		}
	).thenReturn();
}

/**
 * Get all exchange rates per search param
 * @returns {Promise<any[]>}
 */
async function getExchangeRateFromDb(){
	let result  = await exchange.findAll();
	let arr = new Arraylist();

	if (result.length < 1){
		return arr.toArray();
	}

	for (let i= 0; i < result.length; i++){
		arr.add(result[i]['dataValues']);
	}
	return arr.toArray();
}

/**
 *
 * @returns {Promise<void>}
 */
async function sync(){
	await exchange.sync({alter:true});
}