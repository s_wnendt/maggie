'use strict';

const config = require('../../util/configs/app.config.util');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

module.exports = {
	getAllCompany:getAllCompany,
	add:add,
};

async function getAllCompany(){
	let result = await Company.findAll();
	return result;
}

async function add(company, state) {
	await Company.upsert({
		company:company,
		state:state
	}).thenReturn();
}
const Company = sequelize.define('company', {
	company: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: true
	},
	state: {
		type: Sequelize.INTEGER,
		defaultValue: Sequelize.INTEGER,
		primaryKey: false
	}
});

exports.close = async () => {
	//await sequelize.close;
};

if (config.SYNC)
	Company.sync({force: config.SYNC_FORCE}).thenReturn();

