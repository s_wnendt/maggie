'use strict';

const config = require('../../util/configs/app.config.util');
let dateHelper = require('../../util/helper/date.helper');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

module.exports = {
	searched: searched,
	addSearch: addSearch
};

/**
 * param is searched into given timestamp
 * @param item
 */
async function searched(item){
	return await Search.findAll({
		where: {
			'searched': item
		}
	}).then(async res => {
		if (res === undefined || res.length < 1) {
			return false;
		}
		let item = res[0].dataValues;

		let wasUpdatedToday = dateHelper.compareDates(new Date(item['updatedAt']).toDateString(), new Date().toDateString());
		return wasUpdatedToday;
	});
}

/**
 * Add or update a search-input into DB
 * @param item
 * @returns {Promise<any>}
 */
async function addSearch(item){
	let existingItem = await Search.findOne({
		where: {
			'searched': item
		}
	});
	if (existingItem === null || existingItem === undefined) {
		return await Search.create({
			searched: item
		});
	}
	await Search.update(
		{
			'searched': item
		},
		{where :
			{
				'searched': item
			}
		}
	).thenReturn();
	//await sequelize.close();
}

const Search = sequelize.define('searches', {
	searched: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
});

exports.close = async () => {
	//await sequelize.close;
};

if (config.SYNC)
	Search.sync({force: config.SYNC_FORCE}).thenReturn();