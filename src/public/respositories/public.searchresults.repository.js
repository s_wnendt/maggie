'use strict';

let Arraylist = require('arraylist');
const config = require('../../util/configs/app.config.util');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.DB, config.USER, config.PW, config.OPTIONS);

module.exports = {
	getAllPerSearchParam:getAllPerSearchParam,
	addElement:addElement
};

/**
 * add an element of search to database
 * @param item
 * @returns {Promise<void>}
 */
async function addElement(item){
	await SearchResult.create(
		item
	);
	//await sequelize.close();
}

/**
 * Gell all per search param
 * @param param
 * @param date
 * @param channel
 * @returns {Promise<any[]>}
 */
async function getAllPerSearchParam(param, date, channel){
	let result  = await SearchResult.findAll({
		where: {
			[Sequelize.Op.and]: [
				{
					part : {
						[Sequelize.Op.like] : '%' + param.toUpperCase() + '%'
					}
				},{
					updatedAt :{
						[Sequelize.Op.gt] : date
					},
				},{
					channel : channel
				}
			]
		},
		order: [ ['updatedAt', 'DESC'] ]
	});
	let arr = new Arraylist();

	if (result.length < 1){
		return arr.toArray();
	}

	arr.add(result[0]);
	for (let i= 1; i < result.length; i++){
		let isIn = false;
		for (let j= 0; j < arr.length; j++){
			if (result[i]['dataValues']['company'].toLowerCase() === arr[j]['dataValues']['company'].toLowerCase()
				&& result[i]['dataValues']['conti'].toLowerCase() === arr[j]['dataValues']['conti'].toLowerCase()
				&& result[i]['dataValues']['part'].toLowerCase() === arr[j]['dataValues']['part'].toLowerCase()
				&& result[i]['dataValues']['mfg'].toLowerCase() === arr[j]['dataValues']['mfg'].toLowerCase()
				&& result[i]['dataValues']['cond'].toLowerCase() === arr[j]['dataValues']['cond'].toLowerCase()
				&& result[i]['dataValues']['channel'].toLowerCase() === arr[j]['dataValues']['channel'].toLowerCase()
			){
				isIn = true;
			}
		}
		if (!isIn)
			arr.add(result[i]);
	}
	return arr.toArray();
}

const SearchResult = sequelize.define('searchresults', {
	company: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	country: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	part: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	mfg: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	cond: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	priceDoll: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	priceEur: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	qtyState: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	qty: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	age: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	description: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	channel: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	},
	conti: {
		type: Sequelize.STRING,
		defaultValue: Sequelize.STRING,
		primaryKey: false
	}
});

exports.close = async () => {
	//await sequelize.close();
};

if (config.SYNC)
	SearchResult.sync({force: config.SYNC_FORCE}).thenReturn();