'use strict';

let logger = require('../../util/logger.util');
let stringUtil = require('../../util/helper/string.helper');
let TAG = 'public.mpn.service';
let start;
let ArrayList = require('arraylist');
let cacheRepository = require('../respositories/public.search.cache.repository');

let itScopeService = require('../../itscope/services/itscope.mpn.service');
let bbService = require('../../brokerbin/services/bb.mpn.service');
let emptyModel = require('../../itscope/model/itscope.mpn.model');
let linkedService = require('../../public/services/public.linked.service');
let big = require('../../itscope/model/itscope.mpn.model');
let productVariationService = require('../../horst/services/horst.productvariation.service');
let sortService = require('../services/public.defaultsorting.service');
let utilHelper = require('../../util/helper/util.helper');
let companyRepo = require('../respositories/public.company.repository');

module.exports = {
	getState:getState,
	getMpn:getMpn
};

/**
 * General Interface
 * @param searchParam
 * @param filter
 * @returns {Promise<*>}
 */
async function getMpn(searchParam, filter){
	emptyModel.init();
	let companyList = await companyRepo.getAllCompany();
	if(stringUtil.isPotentialInjection(searchParam)){
		logger.log('injection', searchParam);
		return emptyModel.getJSON();
	}
// console.log('public mpn');
	if (filter.blacklisted === undefined || filter.blacklisted === null || filter.blacklisted.length < 4){
		filter.blackCompany = [];
	} else {
		try {
			filter.blackCompany = JSON.parse(filter.blacklisted);
		} catch (e) {
			logger.err(TAG, 'Blacklisting ' + e);
			filter.blackCompany = [];
		}
	}

	if (filter.filter_tree === undefined && filter.filter_list !== undefined){
		filter.filter_tree = [];
		if (filter.filter_list === null || filter.filter_list.length < 2){
			filter.filter_list = '[]';
		}
		let tmp = JSON.parse(filter.filter_list);
		for(let i= 0; i < tmp.length; i++){
			if (tmp[i].value === true){
				let arr = (tmp[i].id + '').split('_');
				filter.filter_tree.push({
					child: arr[3],
					parent: arr[1]
				});
			}
		}
	}
	start = new Date().getMilliseconds();

	let arr = await linkedService.getLinkedMpnPerMpn(searchParam);
	let arrayList = new ArrayList();

	for (let i = 0; i < arr.length; i++){
		let item = arr[i];
		let isChached = await cacheRepository.searched(item.toLowerCase());
		if (!isChached){
			await cacheRepository.addSearch(item.toLowerCase());
		}
		await cacheRepository.close;
		let [its, bb] = await Promise.all(
			[
				itScopeService.searchMpn(item, filter, isChached),
				bbService.searchMpn(item, filter, isChached),
			]
		).catch(function (error) {
			logger.err(TAG, 'Promise-Error:' + error);
		});

		its = JSON.parse(its);
		bb = JSON.parse(bb);

		for(let i = 0; i< its.data.length; i++){
			if (arrayList.contains(its.data[i]))
				continue;
			arrayList.add(its.data[i]);
		}
		for(let i = 0; i< bb.data.length; i++){
			if (arrayList.contains(bb.data[i]))
				continue;
			arrayList.add(bb.data[i]);
		}
	}
	await big.init();

	for (let item of arrayList){
		item.companyState = getCompanyState(item.company.toLowerCase(),companyList);
		big.push(item);
	}

	return await buildModel(big, filter);
}

function getCompanyState(comp, list){
	let state = 0;
	for( let i = 0; i < list.length; i++){
		if (comp.toLowerCase() === list[i].company.toLowerCase()){
			state = list[i].state;
			break;
		}
	}

	return state;
}

/***
 * add HorstInformation after filtering (to spare calc-time)
 * @param data
 * @returns {undefined}
 */
async function addHorstInformation(data) {
	let model = new ArrayList();
	let founded = false;
	for(let i = 0; i< data.length; i++) {
		if (!data[i]['company'].toLowerCase().includes('cybertrading')){
			data[i].salesinformation = '';
			data[i].instock = '';
			data[i].comment = '';
			continue;
		}
		for (let j = 0; j < model.length; j++){
			if (model[j]['part'] === data[i]['part'] && model[j]['cond'] === data[i]['cond']){
				data[i].salesinformation = model[j]['salesInfo'];
				data[i].instock = model[j]['instock'];
				data[i].comment = model[j]['comment'];
				founded = true;
				break;
			}
		}
		if (founded)
			continue;

		let item = await productVariationService.getProductVariation(data[i]['part'], data[i]['cond']);
		if (item === undefined) {
			data[i].salesinformation = '';
		} else {
			model.add({
				part: data[i]['part'],
				cond: data[i]['cond'],
				salesInfo: item.Value['SalesInformation'],
				instock: item.Value['ActualQuantity'],
				comment: item.Value['Comment']
			});
			data[i].salesinformation = item.Value['SalesInformation'];
			data[i].instock = item.Value['ActualQuantity'];
			data[i].comment = item.Value['Comment'];
		}
	}
	return data;
}

/***
 *
 * @param big
 * @param filter
 * @returns {Promise<string>}
 */
async function buildModel(big, filter){
	big = buildJsTreeFilter(JSON.parse(big.getJSON()));
	big.data = sortService.buildDefaultSort(big.data, filter.default_sorting, filter.stateOfCall, filter.cyberfirst);
	let end = new Date().getMilliseconds();
	if (filter.skip_horst !== 'true')
		big.data = await addHorstInformation(big.data);
	logger.log('searchTime', Math.abs((end - start)));
	return JSON.stringify(big);
}

/***
 * Build the native Treefilter
 * @param amp
 * @returns {*}
 */
function buildJsTreeFilter(amp){
	amp.jstree = [];

	//Condition
	let cond = {
		'id' : utilHelper.guid(),
		'parent' : '#', 'text' : 'Condition' };
	amp.jstree.push(cond);
	for (let i = 0; i < amp.sum.condition.length; i++){
		let item = {};
		item.id = require('../../util/helper/util.helper').guid();
		item.parent = cond.id;
		item.text = amp.sum.condition[i];
		amp.jstree.push(item);
	}

	//MFG
	let mfg = {
		'id' : utilHelper.guid(),
		'parent' : '#', 'text' : 'MFG' };
	amp.jstree.push(mfg);
	for (let i = 0; i < amp.sum.manufacturer.length; i++){
		let item = {};
		item.id = utilHelper.guid();
		item.parent = mfg.id;
		if (amp.sum.manufacturer[i].length > 17)
			item.text = amp.sum.manufacturer[i].substring(0,17) + '...';
		else
			item.text = amp.sum.manufacturer[i];
		amp.jstree.push(item);
	}

	//Region
	let region = {
		'id' : utilHelper.guid(),
		'parent' : '#', 'text' : 'Region' };
	amp.jstree.push(region);
	for (let i = 0; i < amp.sum.region.length; i++){
		let item = {};
		item.id = utilHelper.guid();
		item.parent = region.id;
		if (amp.sum.region[i].length > 15)
			item.text = amp.sum.region[i].substring(0,15) + '...';
		else
			item.text = amp.sum.region[i];
		amp.jstree.push(item);
	}

	//qtyState
	let qtyState = {
		'id' : utilHelper.guid(),
		'parent' : '#', 'text' : 'Qty-State (only IT)' };
	amp.jstree.push(qtyState);
	for (let i = 0; i < amp.sum.qtyState.length; i++){
		let item = {};
		item.id = utilHelper.guid();
		item.parent = qtyState.id;
		if (amp.sum.qtyState[i].length > 15)
			item.text = amp.sum.qtyState[i].substring(0,15) + '...';
		else
			item.text = amp.sum.qtyState[i];
		amp.jstree.push(item);
	}

	return amp;
}

/***
 * get the state of an product
 */
function getState(){
	let result = {};
	return JSON.stringify(result);
}
