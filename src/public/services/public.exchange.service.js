const exchangeRepo = require('../respositories/public.exchange.repository');
const exchanges = require('../../ecb/services/ecb.exchange.service');

module.exports = {
	getExchangeRate: getExchangeRate
};

/**
 * Returns the current exchange rate from euro to the passed currency
 * @param currency
 * @returns {Promise<number>}
 */
async function getExchangeRate(currency){
	let targetCurrency = currency;
	let currencyRate = 1;
    
	await exchangeRepo.sync();
    
	let exchangeList;
	let exchangeRatesAreUpToDate = await exchangeRepo.exchangeRatesAreUpToDate(targetCurrency);

	if(!exchangeRatesAreUpToDate){
		exchangeList = await exchanges.getExchanges();
	}
	else{
		exchangeList = await exchangeRepo.getExchangeRateFromDb();
	}

	if(!exchangeRatesAreUpToDate) {
		for (let i = 0; i < exchangeList.length; i++) {
			await exchangeRepo.updateExchangeRate(exchangeList[i].currency, exchangeList[i].rate);
		}
	}

	for(let i = 0; i < exchangeList.length; i++){
		if (exchangeList[i].currency === targetCurrency){
			currencyRate = exchangeList[i].rate;
			break;
		}
	}
    
	return currencyRate;
}