'use strict';

let repo = require('../../public/respositories/public.linked.repository');
let ArrayList = require('arraylist');

module.exports = {
	getLinkedMpnPerMpn:getLinkedMpnPerMpn
};

async function getLinkedMpnPerMpn(mpn) {
	let workingList = await repo.getLinked(mpn, 0);
	let result = new ArrayList();

	for(let i = 0; i < workingList.length; i++){
		let item = workingList[i];
		if(!result.contains(item.dataValues.child)){
			result.add(item.dataValues.child);
		}
		if(!result.contains(item.dataValues.parent)){
			result.add(item.dataValues.parent);
		}
	}

	if (result.length === 0)
		result.push(mpn);
	await repo.close;
	return result.toArray();
}