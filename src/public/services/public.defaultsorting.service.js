'use strict';

let logger = require('../../util/logger.util');
let TAG = 'public.defaultsorting.service';

module.exports = {
	buildDefaultSort:sort
};

let _sorting;
let _biggerThanANumber;
let _cyberfirst;

/**
 * Sorting
 * @param model
 * @param sorting
 * @param stateOfCall
 * @param cyberfirst
 * @returns {*}
 */
function sort(model, sorting, stateOfCall, cyberfirst){
	if (!Array.isArray(model)){
		return [];
	}
	if(sorting !== undefined){
		_biggerThanANumber = (stateOfCall === 'true');
		_sorting = sorting;
		_cyberfirst = cyberfirst;
		model.sort(compare);
	} else if(cyberfirst === 'true'){
		model.sort(compareCyberFirst);
	}

	return model;
}

/**
 * Compare 2 items
 * @param a
 * @param b
 * @returns {number}
 */
function compare(a,b){
	let asc1 = parseInt(_sorting.sort1_asc_item);
	let asc2 = parseInt(_sorting.sort2_asc_item);
	let asc3 = parseInt(_sorting.sort3_asc_item);
	let asc4 = parseInt(_sorting.sort4_asc_item);
	let asc5 = parseInt(_sorting.sort5_asc_item);

	if(_biggerThanANumber === undefined || _biggerThanANumber === null){
		_biggerThanANumber = false;
	}

	let result;

	if (_cyberfirst === 'true'){
		let cyber = compareCyberFirst(a,b);
		if (cyber !== 0){
			return cyber;
		}
	}
	result = abstractComparisionLogic(a,b, _sorting.sort1_item, asc1, _biggerThanANumber);
	if(result === 0){
		result = abstractComparisionLogic(a,b, _sorting.sort2_item, asc2, _biggerThanANumber);
		if(result === 0){
			result = abstractComparisionLogic(a,b, _sorting.sort3_item, asc3, _biggerThanANumber);
			if(result === 0){
				result = abstractComparisionLogic(a,b, _sorting.sort4_item, asc4, _biggerThanANumber);
				if(result === 0){
					return abstractComparisionLogic(a,b, _sorting.sort5_item, asc5, _biggerThanANumber);
				} else {
					return result;
				}
			} else {
				return result;
			}
		} else {
			return result;
		}
	} else {
		return result;
	}
}

function abstractComparisionLogic(a,b, sortItem, ascItem, _biggerThanANumber){
	if(sortItem === undefined || ascItem === undefined)
		return 0;

	let tmp1 = a[sortItem];
	let tmp2 = b[sortItem];

	if (tmp1 === undefined ||tmp2 === undefined)
		return 0;

	if (!isNaN(tmp1) && !isNaN(tmp2)){
		tmp1 = parseFloat(tmp1);
		tmp2 = parseFloat(tmp2);
	} else {
		tmp1 = changeCallIntoANumber(_biggerThanANumber, tmp1);
		tmp2 = changeCallIntoANumber(_biggerThanANumber, tmp2);
	}

	if (tmp1 < tmp2)
		return ascItem ===1 ?-1:1;
	if (tmp1 > tmp2)
		return ascItem ===1 ?1:-1;
	if (tmp1 === tmp2){
		return 0;
	}
}

/***
 * Compare companies per Cybertrading
 * @param a
 * @param b
 * @returns {number}
 */
function compareCyberFirst(a,b){
	let comp1 = a['company'];
	let comp2 = b['company'];

	let tmp1 = 0;
	let tmp2 = 0;


	if (comp1.toLowerCase().includes('cybertr')){
		tmp1 = 1;
	}
	if (comp2.toLowerCase().includes('cybertr')){
		tmp2 = 1;
	}
	return tmp2 - tmp1;
}

/***
 * Simple function to make a call "comparable"
 * @param isBiggerThenANumber
 * @param item
 * @returns {*}
 */
function changeCallIntoANumber(isBiggerThenANumber, item){
	let result = item;
	if (item.toLocaleLowerCase() === 'call'){
		if (isBiggerThenANumber) {
			return Number.MAX_VALUE;
		} else {
			return -1;
		}
	}

	return result;
}

