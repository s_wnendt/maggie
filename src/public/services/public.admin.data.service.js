'use strict';

module.exports = {
	broker:broker,
};

async function broker(action, model) {
	if(action === 'ref'){
		let variationService = require('../../horst/services/horst.productvariation.service');
		return await variationService.getHistoryData(model);
	}
	if(action === 'logic'){
		let variationService = require('../../horst/services/horst.productvariation.service');
		return await variationService.getLogicErrors();
	}
}