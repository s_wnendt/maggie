'use srtict';

let Arraylist = require('arraylist');
let companyRepository = require('../respositories/public.company.repository');

module.exports = {
	addCompanyAndSate:addCompanyAndSate,
	getAll:getAll
};

async function addCompanyAndSate(company, state){
	if (company.length < 1)
		return false;
	if (state < 0 || state > 3){
		return false;
	}
	await companyRepository.add(company, state);
	let result = false;
	return !result;
}

async function getAll(){
	let result = await companyRepository.getAllCompany();
	return result;
}
