/* eslint-disable no-undef,no-unused-vars */
$('document').ready(function(){
	$('#panel').empty().append(
		getLogicGrid()
	);
	loadLogicGrid();
});

function getLogicGrid(){
	let result = $('<div>').attr('id','logicGrid').jsGrid({
		width: '100%',
		height: '400px',

		inserting: false,
		editing: false,
		sorting: false,
		paging: false,

		controller: {
			loadData: function(filter) {
				let d = $.Deferred();
				$.ajax({
					method: 'POST',
					url: '/admin/data',
					data: { action: 'logic'},
					dataType: 'json',
					beforeSend: function (xhr) {
						xhr.setRequestHeader ('Authorization', 'Basic ' + getKey());
					},
					success: function(response) {
						d.resolve(response);
					},
					error: function () {
						alert('Bitte einloggen');
						d.reject();
					}
				});
				return d.promise();
			}
		},

		fields: [
			{ name: 'refCode', title:'RefCode', type: 'text', width: 150, },
			{ name: 'user', title: 'Nutzer',type: 'text', width: 50 },
			{ name: 'reason', title:'Grund', type: 'text', width: 200 },
			{ name: 'timestamp', type: 'text', title: 'Entdeckt',},
		]
	});
	return result;
}

function loadLogicGrid(){
	$('#logicGrid').jsGrid('loadData');
}