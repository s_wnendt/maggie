'use strict';

let path = require('path');
let permission = require('../../user/services/user.permission.service');
let userRole = require('../../user/options/user.userrole.options');
let companyService = require('../services/public.company.service');


module.exports = {
	oldAction: oldAction,
	indexAction: indexAction,
	getAction: getAction,
	adminAction: adminAction,
	adminDataAction: adminDataAction,
	companyAction:companyAction
};

/**
 * routing for index.html
 * @param req
 * @param res
 */
function oldAction(req, res){
	res.sendFile(path.join(__dirname + '/../views/index_old.html'));
}

/**
 * routing for admin.html
 * @param req
 * @param res
 */
function indexAction(req, res){
	res.sendFile(path.join(__dirname + '/../views/index.html'));
}

/**
 * routing for the admin Panel
 * @param req
 * @param res
 */
function adminAction(req, res) {
	res.sendFile(path.join(__dirname + '/../views/config.html'));
}

/**
 * general API-Call
 * @param req
 * @param res
 */
async function getAction(req, res){
	let auth = req.get('authorization');
	if (!auth) {
		return res.status(403).send({});
	}
	let emptyModel = require('../../itscope/model/itscope.mpn.model');
	emptyModel.init();

	if (!(await permission.hasPermission(auth, userRole.user, 'normal-call'))){
		return res.status(200).json(emptyModel.getJSON());
	}

	let service = require('../services/public.mpn.service');
	if (req.body.action === null || req.body.action === undefined) {
		res.status(200).send(emptyModel.getJSON());
		return;
	}

	if (req.body.search === null || req.body.search === undefined) {
		res.status(200).send(emptyModel.getJSON());
		return;
	}

	let action = req.body.action;
	if (action === 'search'){
		let s = await service.getMpn(req.body.search.trim(), req.body.filter)
			.then(t => {
				return t;
			});
		res.status(200).send(s);
	} else if (action === 'state'){
		let s = await (service.getState()
			.then(t => {
				return t;
			}));
		res.status(200).send(s);
	} else {
		res.status(403).send('invalid Action');
	}
}

/**
 * Admin-Data-interface
 * @param req
 * @param res
 */
async function adminDataAction(req, res) {
	let service = require('../services/public.admin.data.service');
	let permission = require('../../user/services/user.permission.service');
	let userRole = require('../../user/options/user.userrole.options');

	let auth = req.get('authorization');
	if (!auth) {
		return res.status(200).send({});
	}

	if (!(await permission.hasPermission(auth, userRole.admin, 'admin-data'))){
		return res.status(200).json({});
	}
	if (req.body.action === null || req.body.action === undefined) {
		res.status(400).send('missing Action');
		return;
	}

	let s = await (service.broker(req.body.action, req.body.model)
		.then(t => {
			return t;
		}));
	res.status(200).send(s);
}

/***
 *
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
async function companyAction(req, res){
	let auth = req.get('authorization');
	if (!auth) {
		return res.status(403).send([]);
	}
	if (req.body.action === null || req.body.action === undefined) {
		return res.status(200).json([]);
	}
	let action = req.body.action;
	if (action === 'add'){
		if (!(await permission.hasPermission(auth, userRole.admin, 'company-call'))){
			return res.status(200).json([]);
		}
		let s = await companyService.addCompanyAndSate(req.body.company.toLocaleLowerCase(), req.body.state)
			.then( t => {
				return t;
			});
		res.status(200).send(s);
	} else if (action === 'get'){
		if (!(await permission.hasPermission(auth, userRole.user, 'company-call'))){
			return res.status(200).json([]);
		}
		let s = await companyService.getAll().then(
			t => {
				return t;
			}
		);
		res.status(200).send(s);
	} else {
		res.status(500).json([]);
	}
}