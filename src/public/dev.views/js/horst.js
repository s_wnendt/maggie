/* eslint-disable no-undef,no-unused-vars */
$('document').ready(function(){
	$('#horstArea').click(() => {
		let token = getKey();
		if (token == null|| token === undefined || token.length < 1){
			return;
		}
		let encodedData = window.atob(token);
		let tap = encodedData.split(':');
		document.getElementById('userInput').value = tap[0];
		document.getElementById('userInputPW').value = tap[1];
	});
});

function getHorstSearch(){
	let horstSearch = $('<form>').attr('class','form-inline').append(
		$('<input>')
			.attr('class','form-control mr-sm-2')
			.attr('type','search')
			.attr('id','inpHorst')
			.attr('placeholder','Ref-Code')
			.attr('aria-label','Suche')
			.keypress(function (e) {
				if (e.which === 13){
					getHorstData(document.getElementById('inpHorst').value);
				}
			})
			.add(
				$('<button>')
					.attr('class','btn btn-outline-success my-2 my-sm-0')
					.attr('type','button')
					.attr('id','btnHorstSearch')
					.text('Suche')
					.click(() => {
						getHorstData(document.getElementById('inpHorst').value);
					})
			)
	);
	let nav = $('<nav>').attr('class','navbar navbar-light bg-light');

	return nav.append(horstSearch).add(
		$('<div>').attr('id','horstGrid').jsGrid({
			width: '97%',
			height: 'auto',

			inserting: false,
			editing: false,
			sorting: false,
			paging: false,

			noDataContent: 'Keinen Artikel gefunden!',

			controller: {
				loadData: function(filter) {
					let d = $.Deferred();
					let refcode = document.getElementById('inpHorst').value;
					if (refcode === undefined || refcode.length < 5){
						return;
					}
					$.ajax({
						method: 'POST',
						url: '/admin/data',
						data: { action: 'ref', model: refcode },
						dataType: 'json',
						beforeSend: function (xhr) {
							xhr.setRequestHeader ('Authorization', 'Basic ' + getKey());
						},
						success: function(response) {
							d.resolve(response);
						},
						error: function () {
							alert('Fehler');
							d.reject();
						}
					});
					return d.promise();
				}
			},

			fields: [
				{ name: 'version', title: 'Version', type: 'number', width: 50,
					itemTemplate: function (value, item) {
						return value;
					}
				},
				{ name: 'user', title: 'User', type: 'text', width: 50,
					itemTemplate: function (value, item) {
						return value;
					}
				},
				{ name: 'timestamp', title: 'Zeitstempel', type: 'text', width: 200,
					itemTemplate: function (value, item) {
						let tmp = (new Date(value.substring(0, 23 )));
						let result = figuredZero(tmp.getDate()) + '.'
							+ figuredZero(tmp.getMonth() +1)
							+ '.' + figuredZero(tmp.getFullYear()) + '  ' +
							figuredZero(tmp.getHours()) + ':' +
							figuredZero(tmp.getMinutes()) + ' Uhr';
						return result;
					}
				},
				{ name: 'bbPrice', title: 'BB-Preis', type: 'number', width: 75,
					itemTemplate: function (value, item) {
						return (Math.round(value * 100) / 100).toFixed(2)  + ' $';
					}
				},
				{ name: 'mpPrice', title: 'MP-Preis',type: 'number', width: 75,
					itemTemplate: function (value, item) {
						return (Math.round(value * 100) / 100).toFixed(2) + ' €';
					}
				},
				{ name: 'marge', title: 'Marge', type: 'number', width: 75,
					itemTemplate: function (value, item) {
						return (Math.round(value * 10000) / 100).toFixed(2) + ' %';
					}
				},
				{ name: 'actualQuantity', title: 'LagerMenge',type: 'number', width: 75,
					itemTemplate: function (value, item) {
						return value;
					}
				},
				{ name: 'bbQuantity', title: 'BB-Menge', type: 'number', width: 75,
					itemTemplate: function (value, item) {
						return value;
					}
				},
				{ name: 'marketPlaceQuantity', title: 'MP-Menge',type: 'number', width: 75,
					itemTemplate: function (value, item) {
						return value;
					}
				},
			]
		})
	);
}

function figuredZero(item){
	if (item < 10)
		return '0' + item;
	return item;
}

function getHorstData(search){
	loadHorstGrid();
}

function loadHorstGrid(){
	$('#horstGrid').jsGrid('loadData');
}