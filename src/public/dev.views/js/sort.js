/* eslint-disable no-undef,no-unused-vars */
$('document').ready(function(){
	validate();
	$('#Sort1').on('change', function () {
		validate();
	});
	$('#Sort1_asc').on('change', function () {
		validate();
	});

	$('#Sort2').on('change', function () {
		validate();
	});
	$('#Sort2_asc').on('change', function () {
		validate();
	});
	$('#Sort3').on('change', function () {
		validate();
	});
	$('#Sort3_asc').on('change', function () {
		validate();
	});
	$('#Sort4').on('change', function () {
		validate();
	});
	$('#Sort4_asc').on('change', function () {
		validate();
	});
	$('#Sort5').on('change', function () {
		validate();
	});
	$('#Sort5_asc').on('change', function () {
		validate();
	});
	$('#save_sort').click(function(e){
		saveSortParam();
	});
	$('#close_sort').click(function(e){
		loadGrid();
	});
	$('#sort_Config').click(function(e){
		loadSortParam();
		validate();
	});
});

function saveSortParam() {
	let sort1_item = document.getElementById('Sort1').value;
	let sort1_asc_item = document.getElementById('Sort1_asc').value;
	let sort2_item = document.getElementById('Sort2').value;
	let sort2_asc_item = document.getElementById('Sort2_asc').value;
	let sort3_item = document.getElementById('Sort3').value;
	let sort3_asc_item = document.getElementById('Sort3_asc').value;
	let sort4_item = document.getElementById('Sort4').value;
	let sort4_asc_item = document.getElementById('Sort4_asc').value;
	let sort5_item = document.getElementById('Sort5').value;
	let sort5_asc_item = document.getElementById('Sort4_asc').value;

	let dto = {};
	if (sort1_item.length > 0){
		$('#Sort1').addClass('is-valid');
		dto.sort1_item = sort1_item;
	} else {
		$('#Sort1').removeClass('is-valid');
	}
	if (sort1_asc_item.length > 0){
		$('#Sort1_asc').addClass('is-valid');
		dto.sort1_asc_item = sort1_asc_item;
	} else {
		$('#Sort1_asc').removeClass('is-valid');
	}

	if (sort2_item.length > 0){
		$('#Sort2').addClass('is-valid');
		dto.sort2_item = sort2_item;
	} else {
		$('#Sort2').removeClass('is-valid');
	}
	if (sort2_asc_item.length > 0){
		$('#Sort2_asc').addClass('is-valid');
		dto.sort2_asc_item = sort2_asc_item;
	} else {
		$('#Sort2_asc').removeClass('is-valid');
	}

	if (sort3_item.length > 0){
		$('#Sort3').addClass('is-valid');
		dto.sort3_item = sort3_item;
	} else {
		$('#Sort3').removeClass('is-valid');
	}
	if (sort3_asc_item.length > 0){
		$('#Sort3_asc').addClass('is-valid');
		dto.sort3_asc_item = sort3_asc_item;
	} else {
		$('#Sort3_asc').removeClass('is-valid');
	}

	if (sort4_item.length > 0){
		$('#Sort4').addClass('is-valid');
		dto.sort4_item = sort4_item;
	} else {
		$('#Sort4').removeClass('is-valid');
	}
	if (sort4_asc_item.length > 0){
		$('#Sort4_asc').addClass('is-valid');
		dto.sort4_asc_item = sort4_asc_item;
	} else {
		$('#Sort4_asc').removeClass('is-valid');
	}

	if (sort5_item.length > 0){
		$('#Sort5').addClass('is-valid');
		dto.sort5_item = sort5_item;
	} else {
		$('#Sort5').removeClass('is-valid');
	}
	if (sort5_asc_item.length > 0){
		$('#Sort5_asc').addClass('is-valid');
		dto.sort5_asc_item = sort5_asc_item;
	} else {
		$('#Sort5_asc').removeClass('is-valid');
	}

	let biggerThenNumber = {};
	biggerThenNumber.isBiggerThenANumber = document.getElementById('callIsGreat').checked;
	$('#group_toggle').addClass('is-valid');
	let cyberfirst = document.getElementById('cybertrading_first').checked;
	Cookies.set('cyberfirst', cyberfirst, { expires: 999 });
	Cookies.set('sort', dto, { expires: 999 });
	Cookies.set('call', biggerThenNumber, { expires: 999 });
}

function loadSortParam() {
	let call = getCall();
	let sort = getSort();
	let cyberfirst= getCyberFirst();

	if (call.isBiggerThenANumber){
		$('#callIsGreatLa').addClass('active');
		$('#callIsTinyLa').removeClass('active');
	} else {
		$('#callIsTinyLa').addClass('active');
		$('#callIsGreatLa').removeClass('active');
	}

	if(sort.sort1_item != null || sort.sort1_item === undefined){
		document.getElementById('Sort1').value = sort.sort1_item;
	}
	if(sort.sort1_asc_item != null || sort.sort1_asc_item === undefined){
		document.getElementById('Sort1_asc').value = sort.sort1_asc_item;
	}

	if(sort.sort2_item != null || sort.sort2_item === undefined){
		document.getElementById('Sort2').value = sort.sort2_item;
	}
	if(sort.sort2_asc_item != null || sort.sort2_asc_item === undefined){
		document.getElementById('Sort2_asc').value = sort.sort2_asc_item;
	}

	if(sort.sort3_item != null || sort.sort3_item === undefined){
		document.getElementById('Sort3').value = sort.sort3_item;
	}
	if(sort.sort3_asc_item != null || sort.sort3_asc_item === undefined){
		document.getElementById('Sort3_asc').value = sort.sort3_asc_item;
	}

	if(sort.sort4_item != null || sort.sort4_item === undefined){
		document.getElementById('Sort4').value = sort.sort4_item;
	}
	if(sort.sort4_asc_item != null || sort.sort4_asc_item === undefined){
		document.getElementById('Sort4_asc').value = sort.sort4_asc_item;
	}

	if(sort.sort5_item != null || sort.sort5_item === undefined){
		document.getElementById('Sort5').value = sort.sort5_item;
	}
	if(sort.sort5_asc_item != null || sort.sort5_asc_item === undefined){
		document.getElementById('Sort5_asc').value = sort.sort5_asc_item;
	}

	if(cyberfirst != null){
		document.getElementById('cybertrading_first').checked = cyberfirst;
	}
}

function validate(){
	let sort1_item = document.getElementById('Sort1').value;
	let sort1_asc_item = document.getElementById('Sort1_asc').value;
	let sort2_item = document.getElementById('Sort2').value;
	let sort2_asc_item = document.getElementById('Sort2_asc').value;
	let sort3_item = document.getElementById('Sort3').value;
	let sort3_asc_item = document.getElementById('Sort3_asc').value;
	let sort4_item = document.getElementById('Sort4').value;
	let sort4_asc_item = document.getElementById('Sort4_asc').value;

	if (sort1_item.length < 2 || sort1_asc_item.length < 1){
		$('#Sort2').attr('disabled', 'disabled');
		document.getElementById('Sort2').value = '';
		$('#Sort2_asc').attr('disabled', 'disabled');
		document.getElementById('Sort2_asc').value = '';
	} else {
		$('#Sort2').removeAttr('disabled');
		$('#Sort2_asc').removeAttr('disabled');
	}

	if (sort1_item.length < 2 || sort1_asc_item.length < 1 || sort2_item.length < 2 || sort2_asc_item.length < 1){
		$('#Sort3').attr('disabled', 'disabled');
		$('#Sort3_asc').attr('disabled', 'disabled');
		document.getElementById('Sort3_asc').value = '';
		document.getElementById('Sort3').value = '';
	} else {
		$('#Sort3').removeAttr('disabled');
		$('#Sort3_asc').removeAttr('disabled');
	}

	if (sort1_item.length < 2 || sort1_asc_item.length < 1
		|| sort2_item.length < 2 || sort2_asc_item.length < 1
		|| sort3_item.length < 2 || sort3_asc_item.length < 1)
	{
		$('#Sort4').attr('disabled', 'disabled');
		$('#Sort4_asc').attr('disabled', 'disabled');
		document.getElementById('Sort4_asc').value = '';
		document.getElementById('Sort4').value = '';
	} else {
		$('#Sort4').removeAttr('disabled');
		$('#Sort4_asc').removeAttr('disabled');
	}

	if (sort1_item.length < 2 || sort1_asc_item.length < 1
		|| sort2_item.length < 2 || sort2_asc_item.length < 1
		|| sort3_item.length < 2 || sort3_asc_item.length < 1
		|| sort4_item.length < 2 || sort4_asc_item.length < 1)
	{
		$('#Sort5').attr('disabled', 'disabled');
		$('#Sort5_asc').attr('disabled', 'disabled');
		document.getElementById('Sort5_asc').value = '';
		document.getElementById('Sort5').value = '';
	} else {
		$('#Sort5').removeAttr('disabled');
		$('#Sort5_asc').removeAttr('disabled');
	}
}

function getCall() {
	return getFromCookie('call');
}

function getSort() {
	return getFromCookie('sort');
}

function getCyberFirst() {
	return getFromCookie('cyberfirst');
}

function getFromCookie(letter){
	let result = Cookies.get(letter);
	if (result === undefined)
		return [];
	return JSON.parse(result);
}