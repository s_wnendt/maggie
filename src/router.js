'use strict';

module.exports =  function(app){
	require('../src/public/public.router')(app);
	require('../src/horst/horst.router')(app);
	require('../src/brokerbin/bb.router')(app);
	require('../src/itscope/itscope.router')(app);
	require('../src/outlook/outlook.router')(app);
	require('../src/reporting/reporting.router')(app);
	require('../src/user/user.router')(app);
};