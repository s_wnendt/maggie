'use strict';

let path = '/its/';

module.exports = function (app) {
	let mpnController = require('./controllers/itscope.mpn.controller');
	app.post(path+'mpn/', mpnController.mpnAction);
};