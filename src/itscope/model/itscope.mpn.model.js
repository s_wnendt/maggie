'use strict';

let ArrayList = require('arraylist');

module.exports = {
	init: init,
	push: push,
	getJSON: getModelAsJSON,
	count:count
};

let data = [];
let sum = {};

/**
 * initiate the minimal data-structure for the model
 */
function init() {
	data = [];
	sum = {};
	sum.condition = new ArrayList();
	sum.conditionCount = [];

	sum.manufacturer = new ArrayList();
	sum.manufacturerCount = [];

	sum.country = new ArrayList();
	sum.countryCount = [];

	sum.region = new ArrayList();
	sum.regionCount = [];

	sum.qtyState = new ArrayList();
	sum.qtyStateCount = [];
}

/**
 * pushs an item to the model
 * @param item
 */
function push(item) {

	buildDataSet(sum.condition, sum.conditionCount, item.cond);
	buildDataSet(sum.manufacturer, sum.manufacturerCount, item.mfg);
	buildDataSet(sum.country, sum.countryCount, item.country);
	buildDataSet(sum.region, sum.regionCount, item.conti);
	buildDataSet(sum.qtyState, sum.qtyStateCount, item.qtyState);
	data.push(item);
}

/**
 * Dataset for the Filter
 * @param list
 * @param countList
 * @param item
 */
function buildDataSet(list, countList, item) {
	if (item === undefined || item.length < 1)
		return;
	if (!list.contains(item)){
		countList.push({key: item, value: 1});
		list.add(item);
	} else {
		for (let i1 = 0; i1 < countList.length; i1 ++){
			if (countList[i1].key === item){
				countList[i1].value = countList[i1].value + 1;
			}
		}
	}
}

/**
 * blub
 * @returns {string}
 */
function getModelAsJSON() {
	let model = {};
	model.data = data;
	model.sum = sum;
	return JSON.stringify(model);
}

/**
 * How thick is the fucking model?
 * @returns {number}
 */
function count(){
	return data.length;
}