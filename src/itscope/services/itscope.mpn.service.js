'use strict';

let logger = require('../../util/logger.util');
let TAG = 'itscope.mpn.service';
let numeral = require('numeral');
let ArrayList = require('arraylist');
let stringUtil = require('../../util/helper/string.helper');
let searchResultRepo = require('../../public/respositories/public.searchresults.repository');
let dateHelper = require('../../util/helper/date.helper');
let constants = require('../../util/statics/constants.statics');
let rp = require('request-promise');
let exchanges = require('../../public/services/public.exchange.service');

module.exports = {
	searchMpn: searchMpn,
};

function getStatusCodeDescription(id) {
	if (id === undefined|| id === null){
		return '';
	}
	switch (id) {
	case '1':
		return 'Lager';
	case '2':
		return 'Zulauf';
	case '3':
		return 'Außenlager';
	case '4':
		return 'Auftrag';
	case '6':
		return 'NV';
	case '8':
		return 'unbekannt';
	default:
		return 'frag Alrik';
	}
}

/**
 *
 * @param model
 * @param search
 * @param filter
 * @returns {Promise<*>}
 */
async function fillFromCache(model, search, filter){
	let list = await searchResultRepo.getAllPerSearchParam(search, dateHelper.diffHoursFromDate(new Date(), constants.CACHING_TIME_IN_HOURS), 'ITS');
	for (let i = 0; i < list.length; i++){
		let g = list[i].dataValues;
		if (isFiltered(g, filter, search)) {
			model.push(validate(g));
		}
	}
	await searchResultRepo.close;
	return model;
}

/**
 * Search for an Item
 * @param searchParam
 * @param filter
 * @param isChached
 * @returns {Promise<string>}
 */
async function searchMpn(searchParam, filter, isChached){
	logger.log(TAG, 'searched for ' + searchParam);
	let model = require('../model/itscope.mpn.model');
	model.init();
	console.log('aus dem ITS Service');
	//alert('hier ITS Services' );
	if (isChached){
		model = await fillFromCache(model, searchParam, filter);
		return model.getJSON();
	}

	/**
	 * Der User-Agent gibt der Api nur seine Herkunft an. Kann also so bleiben.
	 * useragent in der Form <applicationCompany>-<applicationName>-<applicationVersion>
	 * z.B. MeineFirma-MeineAnwendung-4.0927
	 * request.UserAgent = "MeineFirma-MeineAnwendung-4.0927"
	 *
	 * //username:password Kombination Base64 codiert mit hilfe von http://www.base64encode.org/
	 * string credentials = <Account-ID>+ ":" + <API Key> ;
	 * authorization = Convert.ToBase64String(Encoding.Default.GetBytes(credentials));
	 *
	 * // Basic Authorization als http Header
	 * request.Headers["Authorization"] = "Basic " + authorization;
	 * */

	let options = {
		url: 'https://api.itscope.com/2.0/products/search/keywords='+encodeURIComponent(searchParam) +'/standard.json',
		headers: {
			'User-Agent': 'HTTP_USER_AGENT',
			// 'Authorization': 'Basic bTEwODExMzpUcjg2S2daX05vbkNIN3Z2Z0Z4NEFlSEZicUl5YlZOWGRqMzBHTkIyV2Vv'
			'Authorization': 'Basic bTEwODExMzpIZTg0bGhEdnpjVXhoeVZoUU5FUUJYWFgxZU1oZEFQMXZYTkttSUJNV2hZ'
		}
	};
	let result = await rp(options)
		.then(async function (repos) {

			let currencyRate = await exchanges.getExchangeRate('USD');

			let obj = JSON.parse(repos);
			let products = obj.product;
			for(let i= 0; i < products.length; i++){
				for(let j= 0; j < products[i]['supplierItems'].length; j++){
					let nativeItem = products[i]['supplierItems'][j];
					let item = {};

					item.company = nativeItem['supplierName'];
					item.country = '';
					item.part = nativeItem['manufacturerSKU'];
					item.mfg = nativeItem['manufacturerName'];
					item.mfg = stringUtil.capitalize(item.mfg);

					item.cond = mapConditions(nativeItem['conditionId']);

					if (nativeItem['price'] > 0){
						item.priceEur = nativeItem['price'];
						item.priceDoll = (item.priceEur * currencyRate) + '';
					} else {
						item.priceEur = 'CALL';
						item.priceDoll = 'CALL';
					}
					item.qtyState = getStatusCodeDescription(nativeItem['stockStatus']);
					item.qty = nativeItem['stock'];
					item.age = dateHelper.calcDays(nativeItem['lastStockUpdate'], new Date());
					item.description = nativeItem['productName'];
					item.channel = 'ITS';
					item.conti = 'Europe';

					let repo = require('../../public/respositories/public.searchresults.repository');
					await repo.addElement(validate(item));

					if (isFiltered(item, filter, searchParam)) {
						model.push(validate(item));
					}
				}
			}
			logger.log(TAG, 'founded: ' + model.count() + ' for ' + searchParam);
			return model.getJSON();
		})
		.catch(function () {
			logger.warning(TAG,  'not found for ' + searchParam);
			//logger.err(TAG,  err.substr(0,255));
			return model.getJSON();
		});
	return result;
}

/***
 * Proof of Blacklisting
 * @param list
 * @param item
 * @returns {*}
 */
function isBlacklisted(list, item){
	if (list === undefined || item === undefined)
		return false;
	return (list.includes(item));
}

/***
 * Filtered the searchstring against an Meta-Filter
 * @param item
 * @param meta
 * @param search
 * @returns {boolean}
 */
function isFiltered(item, meta, search) {
	if (meta === undefined || meta === null)
		return true;

	if (item.part === undefined || item.part === null || item.part.length < 1)
		return false;

	if (isBlacklisted(meta.blackCompany, item.company))
		return false;

	if (meta.filter_tree !== undefined)
		if(!filteredByTree(meta.filter_tree, item))
			return false;

	let result = true;
	if (!proofOfIncludes(item.company, meta.company))
		result = false;
	if (!proofOfIncludes(item.conti, meta.conti))
		result = false;
	if (!proofOfIncludes(item.qtyState, meta.qtyState))
		result = false;
	if (!proofOfIncludes(item.country, meta.country))
		result = false;
	if (!proofOfIncludes(item.part, meta.part))
		result = false;
	if (!proofOfIncludes(item.mfg, meta.mfg))
		result = false;
	if (!proofOfIncludes(item.cond, meta.cond))
		result = false;
	if (!proofOfIncludes(item.description, meta.description))
		result = false;
	if (meta.cyber_off === 'true' || meta.cyber_off === true){
		if (proofOfIncludes(item.company, 'cybertrading'))
			result = false;
	}
	if (meta.exact_match === 'true' || meta.exact_match === true){
		if (!(item.part.toLowerCase() === search.toLowerCase()))
			result = false;
	}
	return result;
}

/**
 * Proofs the if a given String includes an String
 * @param item
 * @param comparable
 * @returns {boolean}
 */
function proofOfIncludes(item, comparable){
	if (item === null || item === undefined){
		return true;
	}
	if (comparable === undefined || comparable === null){
		return true;
	}
	return item.toLowerCase().includes(comparable.toLowerCase());
}

/***
 * Simple Validation of the IT-Scope given Object
 * @param obj
 * @returns {*}
 */
function validate(obj){
	obj.company =       stringUtil.replacer(obj.company);
	obj.country =       stringUtil.replacer(obj.country);
	obj.part =          stringUtil.replacer(obj.part);
	obj.mfg =           stringUtil.replacer(obj.mfg);
	obj.cond =          stringUtil.replacer(obj.cond);
	obj.priceDoll =     stringUtil.replacer(obj.priceDoll);
	obj.priceEur =      stringUtil.replacer(obj.priceEur);
	obj.qty =           stringUtil.replacer(obj.qty);
	obj.age =           stringUtil.replacer(obj.age);
	obj.description =   stringUtil.replacer(obj.description);
	obj.qtyState =   	stringUtil.replacer(obj.qtyState);

	if (obj.priceDoll !== 'CALL')
		obj.priceDoll = numeral(obj.priceDoll).format('0.00');
	if (obj.priceEur !== 'CALL')
		obj.priceEur = numeral(obj.priceEur).format('0.00');

	return obj;
}

/**
 * Filter Stuff from JsTree
 * @param filter
 * @param item
 * @returns {boolean}
 */
function filteredByTree(filter, item){
	let logicList_condition = new ArrayList();
	let logicList_MFG = new ArrayList();
	let logicList_conti = new ArrayList();
	let logicList_qtyS = new ArrayList();

	for (let i= 0; i < filter.length; i++) {
		if (filter[i]['parent'] === undefined){
			continue;
		}
		if (filter[i]['parent'] === 'Condition'){
			logicList_condition.add(filter[i]['child']);
		} if (filter[i]['parent'] === 'MFG'){
			logicList_MFG.add(filter[i]['child']);
		} if (filter[i]['parent'] === 'Region') {
			logicList_conti.add(filter[i]['child']);
		} if (filter[i]['parent'] === 'Qty-State (only IT)') {
			logicList_qtyS.add(filter[i]['child']);
		}
	}
	let cond = true;
	if(logicList_condition.size() > 0){
		cond = logicList_condition.contains(item.cond);
	}
	let mfg = true;
	if(logicList_MFG.size() > 0){
		mfg = logicList_MFG.contains(item.mfg);
	}
	let conti = true;
	if(logicList_conti.size() > 0){
		conti = logicList_conti.contains(item.conti);
	}

	let qtyState = true;
	if(logicList_qtyS.size() > 0){
		qtyState = logicList_qtyS.contains(item.qtyState);
	}

	return cond && mfg && conti && qtyState;
}

/**
 * Mapping tool to the conditions from ITscope to Brokerbin
 * @param item
 * @returns {*}
 */
function mapConditions(item) {
	if(item === undefined){
		return '';
	}
	switch(item) {
	case '1':
		return 'NEW';
	case '2':
		return 'USED';
	case '3':
		return 'NOB';
	case '4':
		return 'REF';
	case '5':
		return 'USED';
	case '6':
		return 'USED';
	case '7':
		return 'NEW';
	case '8':
		return 'ESD';
	case '9':
		return 'ESD';
	case '10':
		return 'NFR';
	case '11':
		return 'BULK';
	case '12':
		return 'OEMR';
	case '13':
		return 'NOB';
	default:
		return item;
	}
}

