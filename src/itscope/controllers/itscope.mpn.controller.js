'use strict';

module.exports = {
	mpnAction: mpnAction
};

/**
 * simple Controller
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
async function mpnAction(req, res){
	let service = require('../services/itscope.mpn.service');
	if (req.body.action === null || req.body.action === undefined) {
		res.status(400).send('missing Action');
		return;
	}

	let permission = require('../../user/services/user.permission.service');
	let userRole = require('../../user/options/user.userrole.options');

	let auth = req.get('authorization');
	if (!auth) {
		return res.status(403).send([]);
	}

	if (!(await permission.hasPermission(auth, userRole.user, 'api-call'))){
		return res.status(403).json([]);
	}

	if (req.body.search === null || req.body.search === undefined) {
		res.status(400).send('missing Name');
		return;
	}

	let action = req.body.action;
	if (action === 'search'){
		let s = await (service.searchMpn(req.body.search, req.body.filter)
			.then(t => {
				return t;
			}));
		res.status(200).send(s);
	} else {
		res.status(403).send('invalid Action');
	}
}