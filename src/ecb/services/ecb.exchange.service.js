let rp = require('request-promise');
let xmlParser = require('xml2json');
let logger = require('../../util/logger.util');

module.exports = {
	getExchanges: getExchanges,
};

/**
 * Ruft die tagesaktuellen Wechselkursdaten von der Website der ECB ab und gibt diese als ein Array aus Objekten zurück.
 * @returns {Promise<*>}
 */
async function getExchanges() {
	console.log('ECB (EZB) wird angefragt.');
	let options = {
		url: 'http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml'
	};
	let xmlOptions = {
		object: true
	};
	return await rp(options)
		.then(function (repos) {
			let obj = xmlParser.toJson(repos, xmlOptions);
			let array = [];
			let arrayKey = 0;

			for(let i in obj){
				arrayKey = i;
				array[arrayKey] = obj[i];
			}

			let result = array[arrayKey];

			for(let i = 0; i < 3; i++){
				if(result.hasOwnProperty('Cube')){
					result = result.Cube;
				}
				else{
					logger.err('ECB', 'Die Wechselkurse konnten nicht ermittelt werden, da sich wahrscheinlich die XML-Struktur geändert hat.');
					return [];
				}
			}

			return result;
		})
		.catch(function (err) {
			return err;
		});
}