'use strict';

let path = '/reporting/';

module.exports = function (app) {
	//cron-api
	let cronController = require('./controllers/reporting.cron.controller');
	app.post(path+'cron/', cronController.controlAction);
};