'use strict';

module.exports = {
	controlAction: controlAction
};

/**
 * Dispatcher for POST-Request
 * @param req
 * @param res
 */
async function controlAction(req, res){
	let service = require('../services/reporting.cron.service');
	if (req.body.action === null) {
		res.status(400).send('missing Action');
	}

	let permission = require('../../user/services/user.permission.service');
	let userRole = require('../../user/options/user.userrole.options');

	let auth = req.get('authorization');
	if (!auth) {
		return res.status(403).send(false);
	}

	if (!(await permission.hasPermission(auth, userRole.admin , 'reporting'))){
		return res.status(403).json(false);
	}

	let action = req.body.action;
	if (action === 'start'){
		service.startCron();
		res.status(200).send(true);
	} else if (action === 'stop'){
		service.stopCron();
		res.status(200).send(true);
	} else if (action === 'once'){
		let task = require('../tasks/reporting.cron.task');
		task.run();
		res.status(200).send(true);
	} else if (action === 'change'){
		service.changeTime();
		res.status(200).send(true);
	} else if (action === 'getState') {
		let state = service.getState();
		res.status(200).send(JSON.stringify(state));
	} else {
		res.status(403).send('invalid Action');
	}
}