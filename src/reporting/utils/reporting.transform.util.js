'use strict';

module.exports  = {
	transformDataSetSales:transformDataSetSales,
	transformDataSetNewRef: transformDataSetNewRef
};

let ArrayList = require('arraylist');

/**
 * transform the fucking Dataset
 * @param set
 * @returns {Promise<ArrayList>}
 */
async function transformDataSetSales(set) {
	let result = new ArrayList();
	let exchange = require('../../horst/services/horst.exchange.service');
	let exchangeLists = await exchange.getExchange();
	let i, currencyRate;

	for (let i = 0; i < exchangeLists['Value'].length; i++){
		if (exchangeLists['Value'][i]['CurrencySymbolFrom'] === '$'
			&& exchangeLists['Value'][i]['CurrencySymbolTo'] === '€'){
			currencyRate = exchangeLists['Value'][i]['CurrencyRate'];
		}
	}

	for(i=0; i < set.length; i++){
		let item = {
			guid: set[i]['VariationGuid'],
			stock: set[i]['ActualQuantity'],
			bbStock: set[i]['Quantity'],
			marketPlacePrice: set[i]['MarketplacePrice'],
			purchasePrice: set[i]['PurchasePrice'] / currencyRate,
			marge: (set[i]['MarketplacePrice'] - set[i]['PurchasePrice'] / currencyRate) / (set[i]['PurchasePrice'] / currencyRate),
			mail: buildCybertradingMail(set[i]['Firstname'], set[i]['Lastname'])
		};
		result.add(item);
	}
	return result;
}

/**
 * transform the Dataset for new/ref Comparision
 * @param set
 * @returns {Promise<ArrayList>}
 */
async function transformDataSetNewRef(set) {
	let result = new ArrayList();
	for (let i = 0; i < set.length; i++){
		let item = {
			mpn: set[i]['ManufacturerProductNumber'],
			forBroker: (set[i]['newQuantity'] > 0 && set[i]['refQuantity'] > 0),
			mail: buildCybertradingMail(set[i]['Firstname'], set[i]['Lastname'])
		};
		result.add(item);
	}
	return result;
}

/**
 * build the cybertrading mail
 * @param first
 * @param sur
 * @returns {string}
 */
function buildCybertradingMail(first, sur){
	let constants  = require('../../util/statics/constants.statics');
	if (first === undefined || sur === undefined){

		return constants.DEFAULT_MAIL;
	}
	return first.substring(0, 1) + '.' + sur + '@' + constants.DOMAIN_CYBER;
}