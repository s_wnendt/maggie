'use strict';

let Arraylist = require('arraylist');
let logger = require('../../util/logger.util');
let reporting = require('../services/reporting.report.service');
let TAG = 'reporting.dispatcher.service';
let constant = require('../../util/statics/constants.statics');
let sendMail = require('sendmail')({
	silent: true
});

module.exports = {
	init:init,
	addAttachment: addAttachment,
	sendMails: sendMails
};

let mails;
let mailsObject;

/**
 * init :D
 */
function init() {
	mails = new Arraylist();
	mailsObject = {};
}

/**
 * add Attachments to a list of mails
 * @param mail
 * @param attachmentName
 * @param attachmentContent
 */
function addAttachment(mail, attachmentName, attachmentContent) {
	if (mails.contains(mail)){
		mailsObject[mail].add({
			filename: attachmentName,
			content: attachmentContent
		});
	} else {
		mails.add(mail);
		mailsObject[mail] = new Arraylist();
		mailsObject[mail].add({
			filename: attachmentName,
			content: attachmentContent
		});
	}
}

/**
 * Send mails
 * @returns {Promise<void>}
 */
async function sendMails(){
	if (mails.length > 0){
		for(let i = 0; i < mails.length; i++){
			await sendMail({
				from: constant.BROADCASTS_MAIL,
				//to: constant.DEV_MAIL,
				to: mails[i],
				cc: [
					constant.HEAD_OF_SALES_MAIL,
					constant.CEO_MAIL,
					constant.DEV_MAIL
				],
				//*/
				replyTo: constant.BROADCASTS_MAIL,
				subject: constant.REPORT_SUBJECT,
				html: await reporting.getAbstractReport() + '',
				attachments: await getAttachments(mails[i])
			}, function (err, reply) {
				if (err){
					logger.err(TAG, err.stack);
				}
			});
		}
	}
}

/**
 * bored interface
 * @param item
 * @returns {Promise<*>}
 */
async function getAttachments(item){
	return mailsObject[item].toArray();
}