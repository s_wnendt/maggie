'use strict';

let sql = require('mssql');
let horstConfig = require('../../util/db/config.horst.prod.db');

module.exports = {
	extract:extractFromHorst,
};

/**
 * extractFromHorst API f
 * @returns {Promise<*>}
 */
async function extractFromHorst(cmd){
	sql.close();
	let pool = await sql.connect(horstConfig);
	let result1 = await pool.request().query(cmd);
	return result1.recordsets[0];
}