'use strict';

let pug = require('pug');
let ArrayList  = require('arraylist');
let productRepo = require('../../public/respositories/public.product.repository');
let constants = require('../../util/statics/constants.statics');

module.exports = {
	getLagermengenReport: getLagermengenReport,
	getAbstractReport:getAbstractReport,
	getSalesMargeReport:getSalesMargeReport,
	getNewRefReport:getNewRefReport,
	getNewProducts:getNewProducts,
};

/**
 * build the stock report
 * @param list
 * @returns {Promise<string>}
 */
async function getLagermengenReport(list){
	let result = '';
	for(let i = 0; i < list.length; i++){
		let item = await getMPN(list[i]['guid']['guid']);
		result = result + item + '\n';
	}
	return result;
}

/***
 * get mpn per guid from Horst
 * @param guid
 * @returns {Promise<Product.mpn|{type, defaultValue, primaryKey}>}
 */
async function getMPN(guid){
	let arr = await productRepo.findPerGUID(guid);
	if (arr === undefined || arr === null)
		return '';
	return arr.dataValues.mpn;
}

/**
 * simple html
 * @returns {Promise<*>}
 */
async function getAbstractReport(){
	let compiledFunction = pug.compileFile(__dirname + '../src/reporting/pug/index.pug', undefined);
	return compiledFunction({
		name: 'Alrik Schnapke'
	});
}

/**
 * build the sales report
 * @param list
 * @returns {Promise<void>}
 */
async function getSalesMargeReport(list){
	let mails = new ArrayList();
	let result = {};
	for (let i = 0; i < list.length; i++){
		let mail = list[i]['guid']['mail'];
		let mpn = await getMPN(list[i]['guid']['guid']);
		if (!mails.contains(mail)){
			mails.add(mail);
			result[mail] = mpn + '\n';
		} else {
			result[mail] = result[mail] + mpn + '\n';
		}
	}
	result.mails = mails;
	return result;
}

/**
 * @param list
 * @returns {Promise<void>}
 */
async function getNewRefReport(list){
	let mails = new ArrayList();
	let result = {};
	for (let i = 0; i < list.length; i++){
		let mail = list[i].forBroker? constants.BROKER_MAIL : list[i].mail;
		let mpn = list[i].mpn;
		if (!mails.contains(mail)){
			mails.add(mail);
			result[mail] = mpn + '\n';
		} else {
			result[mail] = result[mail] + mpn + '\n';
		}
	}
	result.mails = mails;
	return result;
}

function getNewProducts(list){
	let result = 'Manufacturer;Partnumber;Listprice;Description;FirstGpl;LastGpl\n';
	for(let i = 0; i < list.length; i++){
		let item = list[i].dataValues;
		result = result + item['manufacturer'] + ';' + item['partnumber']+ ';' + item['listprice']+ ';' + item['description'] + '\n';
	}
	return result;
}