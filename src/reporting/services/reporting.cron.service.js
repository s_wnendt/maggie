'use strict';

let logger = require('../../util/logger.util');

module.exports = {
	startCron: startCron,
	stopCron: stopCron,
	changeTime: changeTime,
	getState: getState
};

let cron;
let task = null;

let stateArray = {};

/**
 * init the service
  */
function init(){
	if (task !== null )
		return;
	cron = require('node-cron');

	stateArray.name = 'Reporting';
	stateArray.state = 'init';

	task = cron.schedule('0 6 * * 1-5', function(){
		buildTask();
	}, false);
}

/**
 * start the specific Cron
 */
function startCron() {
	init();
	buildTask();
	task.start();
	stateArray.state = 'started';
}

/**
 * stopped the Cron
 */
function stopCron() {
	init();
	task.stop();
	stateArray.state = 'stopped';
}

/**
 *  change timeinterface as a cronstring
 */
function changeTime(cronString) {
	if (!cron.validate(cronString))
		return;
	init();
	stopCron();
	task = cron.schedule(cronString, function(){
		buildTask();
	}, false);
}

/**
 * get state of the cron
 */
function getState() {
	init();
	return stateArray;
}

/**
 * build the task
 */
function buildTask(){
	logger.log('Cron','Reporting');
	let todo = require('../tasks/reporting.cron.task');
	todo.run();
}