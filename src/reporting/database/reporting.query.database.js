'use strict';

module.exports = {
	getSalesExtract:getSalesExtract,
	getRefNewComparison:getRefNewComparison
};

function getSalesExtract(){
	return 'Select\n' +
		'  pv.ReferenceCode,\n' +
		'  pv.VariationGuid,\n' +
		'  pv.ActualQuantity,\n' +
		'  pv.Quantity,\n' +
		'  pv.Marge,\n' +
		'  pv.MarketplacePrice,\n' +
		'  pv.PurchasePrice,\n' +
		'  worker.Lastname as \'Lastname\',\n' +
		'  worker.Firstname as \'Firstname\'\n' +
		'from\n' +
		'  BaseData.ProductVariation as pv\n' +
		'  join BaseData.Product as p on p.Id = pv.ProductId\n' +
		'  join BaseData.Manufacturer as m on p.ManufacturerId = m.Id\n' +
		'  join BaseData.ManufacturerGroupManufacturer as mg2m on mg2m.ManufacturerId = m.Id\n' +
		'  join BaseData.ManufacturerGroup as mg on mg.Id = mg2m.ManufacturerGroupId\n' +
		'  join UserData.[User] as worker on worker.Id = mg.WorkerId\n' +
		'where 1=1\n' +
		'  and DateDeleted is NULL\n' +
		'  and PurchasePrice is not NULL\n' +
		'  and PurchasePrice > 0\n' +
		'  and MarketplacePrice is not NULL\n' +
		'  and MarketplacePrice > 0\n' +
		'  and ActualQuantity is not null\n' +
		'  and VariationGuid not in (Select ChildVariationGuid from BaseData.ProductVariationMapping)';
}

function getRefNewComparison(){
	return 'Select\n' +
		'  p.ManufacturerProductNumber,\n' +
		'  (new.MarketplaceQuantity + new.Quantity) as \'newQuantity\',\n' +
		'  (ref.MarketplaceQuantity + ref.Quantity) as \'refQuantity\',\n' +
		'  --new.PurchasePrice - ref.PurchasePrice as \'BB_new_minus_ref\',\n' +
		'  --new.MarketplacePrice - ref.MarketplacePrice as \'market_new_minus_ref\',\n' +
		'  --count(5)\n' +
		'  worker.Lastname as \'Lastname\',\n' +
		'  worker.Firstname as \'Firstname\'\n' +
		'from BaseData.ProductVariation as new\n' +
		'  join BaseData.ProductVariation as ref on new.ProductId = ref.ProductId\n' +
		'  join BaseData.Product as p on p.Id = new.ProductId\n' +
		'  join BaseData.Manufacturer as m on p.ManufacturerId = m.Id\n' +
		'  join BaseData.ManufacturerGroupManufacturer as mg2m on mg2m.ManufacturerId = m.Id\n' +
		'  join BaseData.ManufacturerGroup as mg on mg.Id = mg2m.ManufacturerGroupId\n' +
		'  join UserData.[User] as worker on worker.Id = mg.WorkerId\n' +
		'where 1=1\n' +
		'  and new.Condition = 1\n' +
		'  and ref.Condition = 2\n' +
		'  and (new.PurchasePrice < ref.PurchasePrice\n' +
		'      or\n' +
		'       new.MarketplacePrice < ref.MarketplacePrice)\n' +
		'  and new.PurchasePrice > 0\n' +
		'  and new.DateDeleted is null\n' +
		'  and ref.DateDeleted is null\n' +
		'  and ref.VariationGuid not in (Select ChildVariationGuid from BaseData.ProductVariationMapping)\n' +
		'  and new.VariationGuid not in (Select ChildVariationGuid from BaseData.ProductVariationMapping)\n';
}
