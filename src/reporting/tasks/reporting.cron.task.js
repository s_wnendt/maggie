'use strict';

let ArrayList = require('arraylist');
let logger = require('../../util/logger.util');
let TAG = 'reporting.cron.task';

module.exports = {
	run:run
};

/**
 * run the reporting Task
 * @returns {Promise<void>}
 */
async function run(){
	let pgImport = require('../../horst/tasks/mpn.task');
	await pgImport.run();

	let linkImport = require('../../horst/tasks/linked.task');
	await linkImport.run();

	let picker = require('../services/reporting.pick.service');
	let dispatcher = require('../services/reporting.dispatcher.service');
	dispatcher.init();

	let productRepo = require('../../horst/repositories/horst.newproduct.repository');
	let newProductList = await productRepo.getNewProducts();

	for (let i = 0; i < newProductList.length; i++){
		let item = newProductList[i].dataValues;
		await productRepo.deleteNewProductsPerGUID(item.guid);
	}

	let sqlManager = require('../database/reporting.query.database');
	let nativeDataSet = await picker.extract(sqlManager.getSalesExtract());
	let transformManager = require('../utils/reporting.transform.util');
	let dataSet = await transformManager.transformDataSetSales(nativeDataSet);

	let lagerMengeMarge = new ArrayList();
	let salesMarge = new ArrayList();

	let tooExpensiveSales = new ArrayList();
	let tooExpensiveBroker = new ArrayList();

	for (let i = 0; i < dataSet.size(); i++) {
		let item = dataSet.get(i);
		if (item['marge'] < 0 && item['stock'] === 0  && item['bbStock'] === 0){
			salesMarge.add({
				guid: item,
				reasonID: 0,
				reason: 'Marge unterschreitet die 0%-Schwelle (LM)'
			});
		}
		if (item['marge'] < -0.05 && (item['stock'] > 0 || item['bbStock'] > 0)){
			lagerMengeMarge.add({
				guid: item,
				reasonID: 1,
				reason: 'Marge unterschreitet die -5%-Schwelle (LM)'
			});
		}
		if (item['marge'] > 1 && item['purchasePrice'] > 100){
			if (item['stock'] === 0  && item['bbStock'] === 0){
				tooExpensiveSales.add({
					guid: item,
					reasonID: 1,
					reason: 'zu Teuer (Sales)'
				});
			} else {
				tooExpensiveBroker.add({
					guid: item,
					reasonID: 1,
					reason: 'zu Teuer (LM)'
				});
			}
		}
	}

	nativeDataSet = await picker.extract(sqlManager.getRefNewComparison());
	let newRefInequality = await transformManager.transformDataSetNewRef(nativeDataSet);

	let constants = require('../../util/statics/constants.statics');

	let builder = require('../services/reporting.report.service');
	if (lagerMengeMarge.length > 0) {
		let lager = await builder.getLagermengenReport(lagerMengeMarge);
		dispatcher.addAttachment(constants.BROKER_MAIL, 'lm_marge_zu_gering.csv', lager);
	}
	/*
	if (tooExpensiveBroker.length > 0) {
		let lager = await builder.getLagermengenReport(tooExpensiveBroker);
		dispatcher.addAttachment(constants.BROKER_MAIL, 'lm_zu_teuer.csv', lager);
	}
	*/
	if (salesMarge.length > 0){
		let lager = await builder.getSalesMargeReport(salesMarge);
		for (let i= 0; i < lager.mails.length; i ++){
			dispatcher.addAttachment(lager.mails[i], 'marge_zu_gering.csv', lager[lager.mails[i]]);
		}
	}
	/*
	if (tooExpensiveSales.length > 0){
		let lager = await builder.getSalesMargeReport(tooExpensiveSales);
		for (let i= 0; i < lager.mails.length; i ++){
			dispatcher.addAttachment(lager.mails[i], 'zu_teuer.csv', lager[lager.mails[i]]);
		}
	}
	*/
	if (newRefInequality.length > 0) {
		let list = await builder.getNewRefReport(newRefInequality);
		for (let i= 0; i < list.mails.length; i ++){
			dispatcher.addAttachment(list.mails[i], 'ref_ist_teurer_als_new.csv', list[list.mails[i]]);
		}
	}
	if (newProductList.length > 0) {
		let workingList = await builder.getNewProducts(newProductList);
		dispatcher.addAttachment(constants.PRODUCTMANAGER_MAIL, 'new_products_gpl.csv', workingList);
	}
	await dispatcher.sendMails();
}