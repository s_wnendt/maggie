describe('bb.mpn.service', function(){
	let service = require('../../src/brokerbin/services/bb.mpn.service');
	describe('filter', function(){
		it('simple proofOfIncludes', function () {
			expect(service.proofOfIncludes('test','a')).toBe(false);
			expect(service.proofOfIncludes('Cisco1848', '1848')).toBe(true);
		});
		it(('null tests'), function() {
			expect(service.proofOfIncludes(null, '1848')).toBe(false);
			expect(service.proofOfIncludes('Cisco1848', null)).toBe(true);
		});
		it(('undefined Tests'), function() {
			expect(service.proofOfIncludes(undefined, '1848')).toBe(false);
			expect(service.proofOfIncludes('Cisco1848', undefined)).toBe(true);
		});
	});
});