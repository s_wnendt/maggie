describe('string.helper', function(){
	let helper = require('../../src/util/helper/string.helper');
	describe('Capitalize', function(){
		it('Null-Test', function () {
			expect(helper.capitalize(undefined)).toEqual('');
			expect(helper.capitalize(null)).toEqual('');
			expect(helper.capitalize('')).toEqual('');
		});
		it('DataType-Test', function () {
			expect(helper.capitalize(3)).toEqual('3');
			expect(helper.capitalize([])).toEqual('');
			expect(helper.capitalize({})).toEqual('');
		});
		it('Work-Test', function() {
			expect(helper.capitalize('Gert')).toEqual('Gert');
			expect(helper.capitalize('GERT')).toEqual('Gert');
			expect(helper.capitalize('gert')).toEqual('Gert');

			expect(helper.capitalize('gert gert')).toEqual('Gert Gert');
			expect(helper.capitalize('gert Gert r')).toEqual('Gert Gert R');
			expect(helper.capitalize('3COM')).toEqual('3com');
		});
	});
	describe('Replacer', function () {
		it('Null-Test', function() {
			expect(helper.replacer(undefined)).toEqual('');
			expect(helper.replacer('')).toEqual('');
			expect(helper.replacer(null)).toEqual('');
			expect(helper.replacer('undefined')).toEqual('');
		});
		it('`-test', function () {
			expect(helper.replacer('gert\'gert')).toEqual('gert`gert');
			expect(helper.replacer('\'gert')).toEqual('`gert');
			expect(helper.replacer('gert\'')).toEqual('gert`');
		});
		it(';-test', function () {
			expect(helper.replacer('gert;gert')).toEqual('gert,gert');
			expect(helper.replacer(';gert')).toEqual(',gert');
			expect(helper.replacer('gert;')).toEqual('gert,');
		});
		it('Double-Content-Test', function () {
			expect(helper.replacer('gert;ge;rt')).toEqual('gert,ge,rt');
			expect(helper.replacer(';ge\'\'rt')).toEqual(',ge``rt');
			expect(helper.replacer('gert;;;')).toEqual('gert,,,');
		});
	});
	describe('Injection-Test', function(){
		it('Null-test', function () {
			expect(helper.isPotentialInjection('')).toBe(false);
			expect(helper.isPotentialInjection([])).toBe(true);
			expect(helper.isPotentialInjection(undefined)).toBe(true);
			expect(helper.isPotentialInjection(null)).toBe(true);
		});
		it('Simple-test', function () {
			expect(helper.isPotentialInjection('SUCHEN')).toBe(false);
			expect(helper.isPotentialInjection('##dawda')).toBe(true);
			expect(helper.isPotentialInjection('dawdas##dawda')).toBe(true);
			expect(helper.isPotentialInjection('mount%20the%20extruder%20to%2')).toBe(true);
			expect(helper.isPotentialInjection('MouseMove(700,%20700,%200)')).toBe(true);
			expect(helper.isPotentialInjection('PSAC05R-050T')).toBe(false);
		});
	});
});