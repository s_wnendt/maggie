describe('util.helper', function(){
	let helper = require('../../src/util/helper/util.helper');
	let ArrayList = require('arraylist');
	describe('GUID', function(){
		it('GUID double Test', function () {
			let list = new ArrayList();
			let done = false;
			for(let i = 0; i < 1000; i++){
				let item = helper.guid();
				if(!(list.contains(item))){
					list.add(item);
				} else {
					done = true;
					break;
				}
			}
			expect(done).toBe(false);
		});
	});
});