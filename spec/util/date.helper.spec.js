describe('Date-Helper-Tests', function () {
	let helper = require('../../src/util/helper/date.helper');
	describe('Day-Diff', function () {
		it('Simple-Test', function () {
			expect(helper.calcDays(new Date(2017,1,1),new Date(2017,1,1))).toEqual('0');
			expect(helper.calcDays(new Date(2017,1,1),new Date(2017,1,2))).toEqual('1');
			expect(helper.calcDays(new Date(2017,1,2),new Date(2017,1,1))).toEqual('1');
		});
		it('Null-Test', function () {
			expect(helper.calcDays(undefined , undefined), 'double undefined ').toEqual('');
			expect(helper.calcDays(undefined, new Date(2017,1,2))).toEqual('');
			expect(helper.calcDays(new Date(2017,1,2), undefined)).toEqual('');
		});
		it('Wrong-Datatype-Test', function () {
			expect(helper.calcDays([] , -123)).toEqual('');
			expect(helper.calcDays({}, Number.MAX_VALUE)).toEqual('');
			expect(helper.calcDays(null, null)).toEqual('');
		});
	});
});