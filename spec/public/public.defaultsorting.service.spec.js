describe('default sorting', function(){
	let service = require('../../src/public/services/public.defaultsorting.service');
	describe('state-control', function(){
		it('undefined Test', function () {
			let data = [];
			let item1 = {
				id:1
			};
			data.push(item1);
			let sorting = {
				sort1_item:'id',
				sort1_asc_item:'1'
			};
			let undefinedResult = service.buildDefaultSort(undefined, undefined);
			expect(undefinedResult).toEqual([],'undefined|undefined');
			let undefinedResult1 = service.buildDefaultSort(data, undefined);
			expect(undefinedResult1).toEqual(data, 'data|undefined');
			let undefinedResult2 = service.buildDefaultSort(undefined, sorting);
			expect(undefinedResult2).toEqual([], 'undefined|sorting');
		});
		it('wrong datatype', function () {

			let undefinedResult = service.buildDefaultSort({}, '');
			expect(undefinedResult).toEqual([], 'Wrong Datatypes');
			let undefinedResult1 = service.buildDefaultSort([], 2);
			expect(undefinedResult1).toEqual([],'Wrong Datatypes');
		});
		it('empty datatypes', function () {
			let data = [];
			let item1 = {
				id:1
			};
			data.push(item1);
			let emptySorting = {
				sort1_item:'',
				sort1_asc_item:''
			};
			let sorting = {
				sort1_item:'id',
				sort1_asc_item:'1'
			};
			let undefinedResult = service.buildDefaultSort([], sorting);
			expect(undefinedResult).toEqual([], 'no Data');
			let undefinedResult1 = service.buildDefaultSort(data, emptySorting);
			expect(undefinedResult1).toBe(data, 'no sorting');
		});
	});
	describe('1 sort test', function () {
		it('simple sort test aufsteigend', function () {
			let data = [];
			let item1 = {
				id: 1
			};
			let item2 = {
				id: 2
			};
			let item3 = {
				id: 3
			};
			data.push(item2);
			data.push(item1);
			data.push(item3);
			let sorting = {
				sort1_item: 'id',
				sort1_asc_item: '1' // aufsteigend
			};
			let sortedModel = service.buildDefaultSort(data, sorting);
			expect(sortedModel[0].id === 1).toBe(true);
			expect(sortedModel[0].id === 2).toBe(false);
			expect(sortedModel[2].id === 3).toBe(true);
		});
		it('simple sort test absteigend', function () {
			let data = [];
			let item1 = {
				id: 1
			};
			let item2 = {
				id: 2
			};
			let item3 = {
				id: 3
			};
			data.push(item2);
			data.push(item1);
			data.push(item3);
			let sorting = {
				sort1_item: 'id',
				sort1_asc_item: '2' // absteigend
			};
			let sortedModel = service.buildDefaultSort(data, sorting);
			expect(sortedModel[0].id === 3).toBe(true);
			expect(sortedModel[0].id === 2).toBe(false);
			expect(sortedModel[2].id === 1).toBe(true);
		});
	});
	describe('2 sort test', function(){
		it('1 ab, 2 auf', function () {
			let data = [];
			let item1 = {
				id: 1,
				name: 'a'
			};
			let item2 = {
				id: 2,
				name: 'a'
			};
			let item3 = {
				id: 2,
				name: 'b'
			};
			data.push(item2);
			data.push(item1);
			data.push(item3);

			let sorting = {
				sort1_item: 'id',
				sort1_asc_item: '1', // absteigend
				sort2_item: 'name',
				sort2_asc_item: '1'
			};
			let a = service.buildDefaultSort(data, sorting);
			expect(a[0].id === 1).toBe(true);
			expect(a[0].name === 'a').toBe(true);
			expect(a[2].name === 'b').toBe(true);
			expect(a[2].id === 2).toBe(true);
		});
	});
});