let gulp = require('gulp');
let htmlmin = require('gulp-htmlmin');
let minifyInline = require('gulp-minify-inline');
let minify = require('gulp-minify');
let replace = require('gulp-replace');
let cssmin = require('gulp-cssmin');
let rename = require('gulp-rename');
let clean = require('gulp-clean');

// vor dem minifiyen!
gulp.task('Dev_SwitchDomain_prod2dev', function(){
	return gulp.src('./src/public/dev.views/admin.html')
		.pipe(replace(
			'localhost', 'apps.service.lokal'
		))
		.pipe(replace(
			'static/css/main.css', 'static/css/main.min.css'
		))
		.pipe(gulp.dest('./src/public/dev.views/'));
});

gulp.task('SwitchDomain_prod2dev', function(){
	return gulp.src('./src/public/views/*.html')
		.pipe(replace(
			'localhost', 'apps.service.lokal'
		))
		.pipe(replace(
			'static/css/main.css', 'static/css/main.min.css'
		))
		.pipe(gulp.dest('./src/public/views/'));
});

// vor dem minifiyen!
gulp.task('domain-prod-2-local', function(){
	return gulp.src('./src/public/dev.views/index.html')
		.pipe(replace(
			'apps.service.lokal', 'localhost'
		))
		.pipe(replace(
			'static/css/main.min.css', 'static/css/main.css'
		))
		.pipe(gulp.dest('./src/public/dev.views/'));
});

gulp.task('html', function(){
	return gulp.src('./src/public/dev.views/*.html')
		.pipe(htmlmin({collapseWhitespace: true}))
		.pipe(minifyInline())
		.pipe(gulp.dest('./src/public/views/'));
});

gulp.task('css', function(){
	return gulp.src('./src/public/dev.views/css/*.css')
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./src/public/views/css/'));
});

gulp.task('norm2minCSS_prod', function () {
	return gulp.src('./src/public/views/index.html')
		.pipe(replace(
			'static/css/main.css', 'static/css/main.min.css'
		))
		.pipe(gulp.dest('./src/public/views/'));
});

gulp.task('js', function(){
	return gulp.src('./src/public/dev.views/js/*.js')
		.pipe(minify({
			ext:{
				src:'-debug.js',
				min:'.js'
			},
			exclude: ['tasks'],
			ignoreFiles: ['.combo.js', '-min.js']
		}))
		.pipe(gulp.dest('./src/public/views/js/'));
});

gulp.task('clean', function(){
	return gulp.src('./src/public/views/*', {read: false})
		.pipe(clean());
});

gulp.task('develop', gulp.series(
	'clean',
	'html',
	'js',
	'css',
	'norm2minCSS_prod',
));

gulp.task('prod', gulp.series(
	'clean',
	'html',
	'js',
	'css',
	'norm2minCSS_prod',
	'SwitchDomain_prod2dev'
));